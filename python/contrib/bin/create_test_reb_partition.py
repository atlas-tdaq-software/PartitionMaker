#!/usr/bin/env tdaq_python
from __future__ import print_function
import os
import sys
import pm.common
from pm.dal import dal, DFdal
from pm.project import Project

import pm.utils

def gen_reb_partition(ros_dbfile, reb_dbfile=None, includes=[]):
  objcache = pm.common.load(includes)

  ros_db = Project(ros_dbfile)

  ros = ros_db.getObject('ROS')
  ros_ids = [r.id for r in ros]
  first_ros_id = sorted(ros_ids)[0]

  # this is rather desperate but at least it won't fail on non-standard names
  # careful!
  det_n_mode = ros_dbfile.split('/')[-1].split('.')[0] # throw away extension & path
  det_n_mode = det_n_mode.replace('ROS-','').replace('robin-datadriven-','')
  # should be in the form DET-reb, DET-edo...

  # create Segment for ROSs (ignores segment from ROS db file, even if present)
  ros_seg = dal.Segment(ros_dbfile.split('/')[-1].split('.')[0] + '-GENERATED')
  ros_seg.IsControlledBy = objcache['DefRC']
  ros_seg.Resources = ros
  def_controller_hostname = os.getenv('DEFAULT_CONTROLLER_HOST')
  try:
    ros_seg.Hosts.insert(0, objcache[def_controller_hostname])
  except KeyError:
    print("Warning: I can't find $DEFAULT_CONTROLLER_HOST in the hosts DB, using fallback hosts...")
    if 'pc-tdq-ros-spare-01.cern.ch' in objcache:
      ros_seg.Hosts.insert(0, objcache['pc-tdq-ros-spare-01.cern.ch'])
    elif 'pc-preseries-dfm-01.cern.ch' in objcache:
      ros_seg.Hosts.insert(0, objcache['pc-preseries-dfm-01.cern.ch'])
    else:
      raise KeyError("I can't find $DEFAULT_CONTROLLER_HOST or the default fallbacks in the hosts DB!")

  segments = [ros_seg]

  if reb_dbfile:
    reb_db = Project(reb_dbfile)

    # uncomment to override DEFAULT_CONTROLLER_HOST
    # ros_seg.DefaultHost = reb_db.getObject('Segment')[0].DefaultHost

    # add segments
    reb_seg = reb_db.getObject('Segment')[0]

    # create ResourceSetORs to bind the ROSs & the REB channels
    # (for quick enable/disable)

    hw_channels = reb_db.getObject('HW_InputChannel')

    binders = []
    for c in hw_channels:
      for r in ros:
        if c.PhysAddress==r.Output.DestinationPort:
          rs = dal.ResourceSetOR(r.id+'-RS')
          rs.Contains = [r, c]
          #print rs
          binders.append(rs)
          break
    ros_seg.Resources = sorted(binders, key=lambda b: b.Contains[0].RunsOn.id)
    segments.append(reb_seg)

  # create Partition object

  partition = dal.Partition('part-'+det_n_mode)
  partition.IPCRef = "$(TDAQ_IPC_INIT_REF)"
  partition.DBPath = "$(TDAQ_DB_PATH)"
  partition.DBName = "$(TDAQ_DB_DATA)"
  partition.DBTechnology = "rdbconfig"
  partition.LogRoot = os.getenv('PARTITION_LOG_ROOT_OVERRIDE')
  if not partition.LogRoot:
    partition.LogRoot = "/tmp/logs/"
  partition.ProcessEnvironment = [objcache['CommonEnvironment'],objcache['External-environment']]
  partition.Parameters = [objcache['CommonParameters']]
  partition.RunTypes = ['Physics']
  partition.Segments = segments
  partition.OnlineInfrastructure = objcache['setup']
  partition.DefaultHost = ros_seg.Hosts[0]
  partition.DefaultTags.append(partition.get('Tag','x86_64-centos7-gcc8-opt'))

  ebcounters = dal.IS_EventsAndRates('EBCounters')
  ebcounters.EventCounter = "DF.ROS.ROSEventBuilder.requestsQueued"
  ebcounters.Rate = "DF.ROS.ROSEventBuilder.requestRateHz"
  ebcounters.Description = "Events seen by REB."

  l1counters = dal.IS_EventsAndRates('L1Counters')
  ##
  l1counters.EventCounter = "DF.ROS." + first_ros_id + ".DataChannel0.fragsreceived"
  l1counters.Rate = "DF.ROS." + first_ros_id + ".DataChannel0.level1RateHz"
  ##
  l1counters.Description = "L1 accepts"

  infsource = dal.IS_InformationSources('DF_ISInfo')
  infsource.LVL1 = l1counters
  infsource.HLT = ebcounters
  partition.IS_InformationSource = infsource
  return partition


if __name__=='__main__':
  from optparse import OptionParser
  usage = "Usage: %prog <daq/segments/ROS/some-ros-db-file> <daq/segments/ROS/reb-segment-file>\n"\
        "\ne.g.\n\t %prog daq/segments/ROS/ROS-PIX-robin-datadriven-reb.data.xml daq/segments/ROS/REB-PIX.data.xml"
  parser = OptionParser(usage)

  (options, args) = parser.parse_args()

  try:
    ros_dbfile = args[0]
    reb_dbfile = args[1]
  except IndexError:
    print(parser.print_help())
    sys.exit()

  includes = [ 'daq/segments/setup.data.xml' ]
  includes += [ros_dbfile, reb_dbfile]

  partition = gen_reb_partition(ros_dbfile, reb_dbfile, includes)

  # save
  import os
  pfile = partition.id + ".data.xml"
  try:
      os.remove(pfile)
      print('previous version of',pfile,' removed')
  except:
      pass
  db = Project(pfile, includes)
  db.addObjects([partition])
