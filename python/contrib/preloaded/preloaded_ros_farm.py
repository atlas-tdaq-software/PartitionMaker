#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :

from __future__ import absolute_import
from builtins import range
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"

##################
# Initialization #
##################

import sys
import pm.multinode
import pm.farm
import pm.common
from eformat import helper
from .preloaded_helpers import generate_preloaded_farm

# PM won't pass parameters to the farms;
# cheat and parse the command line directly
# beware of this when modifying the arguments!
ros_mode = None
for i in range(len(sys.argv)):
  if sys.argv[i] == '--ros-mode': ros_mode = sys.argv[i+1]

assert 'preload' in ros_mode, "Error: farm can only be used in preloaded mode!"

#############
# Filtering #
#############

hostfile = "daq/hw/hosts-ros.data.xml"
hosts = pm.farm.load(hostfile, short_names=True)
#remove ComputerSet
hosts = dict([(k,hosts[k]) for k in hosts if hosts[k].isDalType('Computer')]) 

# filter by host list
#host_list = [ \
    #    'pc-fwd-ros-bcm-00'
    #]
#hosts = pm.farm.filter_hosts_by_list(hosts, host_list)

import os
import pm.farm
base = os.path.dirname(os.path.abspath(__file__))
# list could be in a file
hosts = pm.farm.filter_hosts_by_file(hosts, base+'/data/rosPc.290311.list')

# or use a pattern
#host_pattern = 'pc-csc-ros-ec[a,c]-00'
#hosts = pm.farm.filter_hosts_by_pattern(hosts, host_pattern)

# filters roblist by subdetector (leave empty for all subdetectors)
#subdetector_filter = [ helper.SubDetector.PIXEL_BARREL,
#    helper.SubDetector.PIXEL_DISK_SIDE,
#    helper.SubDetector.PIXEL_B_LAYER, ]
subdetector_filter = []
# subdetector groups: channels of all subdectors in the same group can live in the same ROS
#groups = [[helper.SubDetector.PIXEL_DISK_SIDE, helper.SubDetector.PIXEL_B_LAYER], [0x67,0x68,0x69,0x6a]]
groups = []

ros_farm = generate_preloaded_farm(hosts, 'data/MC.roblist', subdetector_filter, groups)


##############
# Parameters #
##############

#list here the include files
includes = [ \
    'daq/segments/ROS/ros-common-config.data.xml',
    'daq/segments/ROS/ros-specific-config-TDQ.data.xml',
    ]

ros_objs = pm.common.load(includes)

ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']

ros_farm['default_host']= None

#use this to override the default DB parameters for the ROS
ros_farm['ros_override_params'] = { \
    'ActionTimeout':10,
    'ProbeInterval':5,
    'FullStatisticsInterval':60, 
    'Parameters':'-a 3',
    'InitTimeout':60,
    'RestartableDuringRun':1,
    }

# use this to override the default DB parameters for the RunControlApplication
# comment the key out if you want to use RunControlTemplateApplication
#ros_farm['rcapp_override_params'] = { \
    #    'ActionTimeout':60,
    #'ProbeInterval':5,
    #'FullStatisticsInterval':60,
    #'IfDies':'Restart',
    #}



ros_farm['name'] = 'PRELOADED'
ros_farm[helper.SubDetector.FULL_SD_EVENT]['name'] = 'PRELOADED'
ros_farm['default_host'] = ros_objs['pc-tdq-ros-spare-00.cern.ch']

ros_farm['mem_pool'] = ros_objs['ROS-MemoryPool-Data']
ros_farm['ros_config'] = ros_objs['ROS-Config-TDQ']

ros_farm['segment_override_params'] = {
    'ProcessEnvironment': [ros_objs['ROSEnvironment']]
    }

#add other options to ros_farm from this point on


