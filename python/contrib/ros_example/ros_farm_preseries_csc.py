#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :

from builtins import range
__author__ = "Giorgos Boutsioukis <georgios.boutsioukis@cern.ch>"
import pm.farm
from eformat import helper


# This is a farm template provided as an example of the basic structure, since
# point1 farms are generated from a CSV file. New farms should follow this
# example instead of the CSV way.

# Checklist to create a new farm
#########################################################################

# hosts db as returned by pm_farm.py
hosts = pm.farm.load('daq/hw/hosts-preseries.data.xml', short_names=True)

includes = [\
    'daq/hw/hosts-preseries.data.xml',
    'daq/segments/ROS/ros-common-config.data.xml',
    'daq/segments/ROS/ros-specific-config-CSC.data.xml',]

ros_objects = pm.common.load(includes)

#ros_farm = {
#    'name' : 'PRESERIES-PIX',
#         ................
#}
ros_farm = {
    'name' : 'PRESERIES-CSC',
    helper.SubDetector.MUON_CSC_ENDCAP_A_SIDE : {
      'name' : 'CSC-ECA' ,
      'ros' : [
        ( hosts['pc-preseries-ros-00'],
          { 'index': 1 ,'modules':[ (1, [ 0x05, 0x07, 0x09, ]), (2, [ 0x11, 0x15, 0x17, ]), (4, [ 0x19, 0x21, ]), ]})
        ]
      },
    helper.SubDetector.MUON_CSC_ENDCAP_C_SIDE : {
      'name' : 'CSC-ECC' ,
      'ros' : [
        ( hosts['pc-preseries-ros-01'],
          { 'index': 2 ,'modules':[ (1, [ 0x05, 0x07, 0x09, ]), (2, [ 0x11, 0x15, 0x17, ]), (4, [ 0x19, 0x21, ]), ]})
        ]
      }
    }

# *4 <---------- Change these objects for your farm
ros_farm['ros_config'] = ros_objects['ROS-Config-CSC']
ros_farm['robin_channel_config'] = ros_objects['RobinDataChannel-Config-CSC'] 
ros_farm['robin_mem_pool'] = ros_objects['ROS-MemoryPool-Data-CSC']
ros_farm['robin_config'] = ros_objects['Robin-Config-CSC']

# *5 <---------- (REB modes only) Select the REB segment host
ros_farm['reb'] = {}

###############################################################################
# The rest can usually be left unchanged ######################################
###############################################################################

# PM won't pass parameters to the farms;
# cheat and parse the command line directly
# beware of this when modifying the arguments!
import sys

ros_mode = None
for i in range(len(sys.argv)):
  if sys.argv[i] == '--ros-mode': ros_mode = sys.argv[i+1]

assert ros_mode, "No --ros-mode? (needed by the ros_farm file!)"

ros_farm['ros_common_config'] = ['daq/segments/ROS/ros-common-config.data.xml']

ros_farm['ros_override_params'] = { \
    'ActionTimeout':10,
    'ProbeInterval':5,
    'FullStatisticsInterval':60, 
    'Parameters':'-a 3',
    'RestartParameters':'-a 3',
    'InitTimeout':60,
    'RestartableDuringRun':1,
    }
# use spare for testing

ros_farm['reb']['tcp_data_out_config_override_params'] = { \
    'SamplingGap':1,
    'TCPBufferSize':32768,
    'ThrowIfUnavailable':0
    }

# REB segment parameters
ros_farm['reb']['rcapp_override_params'] = { \
    'ActionTimeout':60,
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'IfExitsUnexpectedly':'Restart',
    }
ros_farm['reb']['rcd_override_params'] = {
    'ProbeInterval':5,
    'FullStatisticsInterval':60,
    'Id':85, # (?) this was hardcoded in the perl template
    }
ros_farm['reb']['rcd_config_override_params'] = {
    'NumberOfRequestHandlers':1,
    }

ros_farm['reb']['input_fragment_type'] = 'ROSFragment'

if ros_mode.endswith('-reb'):
  # REB mode only
  ros_farm['ros_override_params']['ReceiveMulticast'] = 0


