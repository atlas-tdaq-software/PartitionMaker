#!/usr/bin/env tdaq_python

"""
This is a script to generate PartitionMaker ros farms from the
old CSV format used by the DBGeneration scripts.
It assumes the presence of the following columns:

DETECTOR_ID: subdetector id
APPLICATION_IP_NAME: ROS hostname
CHANNEL_ID: channel #

"""
from __future__ import division
from __future__ import print_function

from builtins import zip
from builtins import filter
from builtins import str
from builtins import next
from past.utils import old_div
import sys
import csv
from eformat import helper
import pm
from pm.dal import dal

APPLICATION_IP_DOMAIN = ".cern.ch"


def get_subdetector_by_id(id):
    if id in list(helper.SubDetector.values.keys()):
        return helper.SubDetector.values[id]
    else:
        return helper.SubDetector.OTHER


def get_ros_host_by_hostname(hostname, ros_hosts):
    if hostname in list(ros_hosts.keys()):
        return ros_hosts[hostname]
    else:
        print("Warning: Host %s not in hostlist, added bogus properties" % hostname)
        return dal.Computer(hostname)


#farm format:
"""
    farm = {
     'default_host': dal.Computer|*,
     'name': string|*
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'name': string|*,
                      'ros': [
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                             ]
                     },
     subdetector_id: {
                      'default_host': dal.Computer|*,
                      'ros': [
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                              (ros_host, [rob_list]),
                             ]
                     },
      ...
    }
new format:
      'ros': [
              (ros_host, [(module, [rob_list], ...]),
              (ros_host, [rob_list]),
              (ros_host, [rob_list]),
             ]
newer format:
               (ros_host, { 'modules': [(<module_id>, [rob_list]),
                                        (<module_id>, ...])
                                        ...
                                       ],
                            'index': <ros_global_index> }

"""
def parse_row(row):
    row_dict = {}
    for t in zip(row, headers):
        c, h = t
        if h.level:
            #prefer to use levels as keys, as var names might be arbitrary
            row_dict[h.level] = h.parseValue(c)
        else: row_dict[h.tag] = h.parseValue(c)
    return row_dict

# the purpose of keeping a 'global index' for each ROS, is to
# ensure that all ROSs in a datafile will be assigned a different
# index (useful for setting ports etc.)
ROS_COUNTER = 0
ALL_ROS = []

def generate_ros_farm(hosts, datafile_path='./data.ros.csv', still_count=[], pattern="", hosts_as_strings=False):
    """
    Generate a ROS farm from a (historic) CSV file

    hosts - [dal.Computer,...] 
      The list of valid hosts

    datafile_path 
    still_count - [dal.Computer,...] 
      This is a list of hosts that were commented
      out and will not be included in the farm, yet the global ROS index will still
      be increased when they are encountered in the CSV.  """

    print("INFO: Generating ROS farm from CSV file at: %s" %datafile_path)
    if hosts_as_strings:
      hosts = {}
    global ROS_COUNTER
    global ALL_ROS
    with open(datafile_path, 'rt') as datafile:
        ros_farm = {}
     
        rows = csv.reader(datafile)

        for row in rows:
            ROW_STARTED_WITH_EXCLMARK = False
            if pattern not in ','.join(row): continue # skip if it doesn't contain the pattern
            if not row or not row[0].strip(): continue # skip empty lines
            if row[0].startswith('#'): continue # comment
            if row[0].startswith('!'):
              ROW_STARTED_WITH_EXCLMARK = True
              row[0] = row[0].lstrip('!')

            subdetector_id, detector, app_suffix, ip_name,\
                channel_idx, module_id = row

            channel_idx = int(channel_idx)
            robinnp_idx = old_div(channel_idx, 12)
            channel_idx = channel_idx % 12
            
            subdetector = helper.SubDetector.FULL_SD_EVENT
            # this just for accounting
            if app_suffix not in ALL_ROS:
              ALL_ROS.append(app_suffix)
              ROS_COUNTER += 1

            if ip_name not in list(hosts.keys()):
              print("WARNING: No host object found for host '%s' (mentioned in the CSV file):\n\ta)You filtered it out on purpose, ignore this warning.\n\tb)The host is not defined in the host database file (check your TDAQ_DB_PATH)." % ip_name)
              continue
            if ROW_STARTED_WITH_EXCLMARK:
              continue # host was excluded; skip csv line

            ros_host = get_ros_host_by_hostname(ip_name, hosts)

            #add row to farm; create any new objects when necessary
            if subdetector not in list(ros_farm.keys()):
              ros_farm[subdetector] = {'ros':[], 'name': 'ALL'}

            if ros_host not in [ tpl[0] for tpl in ros_farm[subdetector]['ros'] ]:
              ros_farm[subdetector]['ros'].append((ros_host,{'modules':[], 'index':ROS_COUNTER, 'name':app_suffix}))

            roshost_tuple = next(h for h in ros_farm[subdetector]['ros'] if h[0] == ros_host)
            module_list = roshost_tuple[1]['modules']
            if robinnp_idx not in [ tpl[0] for tpl in module_list]:
              module_list.append( (robinnp_idx, []) )

            module_tuple = next(m for m in module_list if m[0] == robinnp_idx)
            channel_list = module_tuple[1]
            robid = int(module_id)+(int(subdetector_id,16)<<16)
            if robid not in channel_list:
                channel_list.append((robid, channel_idx))
            else:
              # this means a) there is a duplicate channel id in the module
              # b) we did something horribly wrong
              raise RuntimeError("Duplicate channel %d (0x%x)"%(robid, robid))

        return ros_farm



def printfarm(farm):
  i = ' '
  indent = ""
  print("ros_farm = {")
  print(indent+i+"'name' : 'CHANGE_THIS_TO_FARM_NAME',")
  for sd in farm:
    indent += i
    if not isinstance(sd, helper.SubDetector): continue
    sdstring = repr(sd)
    sdstring = 'helper.' + str.join('.', sdstring.split('.')[1:])

    print(indent, sdstring, ':', '{')

    indent += i
    print(indent, repr('name'), ':', repr(farm[sd]['name']),',')
    print(indent, repr('ros') + ' : [')

    indent += i
    for tuple in farm[sd]['ros']:
      host = tuple[0]
      if isinstance(host, list): hostname = host[0]
      else: hostname = host.id.split('.')[0]
      rosdict = tuple[1]
      print(indent+"(hosts["+repr(hostname)+"],\n"\
          +indent+i+" {'index':", rosdict['index'], " 'name':", rosdict['name'], ",'modules':[", end=' ')
      for mtuple in rosdict['modules']:
        module_index = mtuple[0]
        module_channels = mtuple[1]
        print('('+str(module_index)+',[', end=' ')
        for channel in module_channels:
          print("0x%02x"%channel+',', end=' ')
        print(']),', end=' ')
      print(']}),')
    indent = indent[:-len(i)]
    print(indent, ']')
    indent = indent[:-len(i)]
    print(indent, '},')
    indent = indent[:-len(i)]
  print(indent, '}')

if __name__ == '__main__':
  from optparse import OptionParser
  usage = "Usage: %prog [options] <path/to/datafile.csv>"
  parser = OptionParser(usage)
  parser.add_option("-g", "--grep", dest="pattern", default="",
      help="Use only lines from the CSV file that contain this pattern (constant pattern, no regex support) ", metavar="PATTERN")
  parser.add_option("-s", "--substitute-hosts", dest="subst_hosts",
      help="Substitute the hosts in the farm with the hostnames in the file (will recycle if not enough)", metavar="<pc.list>")

  (options, args) = parser.parse_args()
  try:
    csvfile = args[0]
  except:
    print(parser.print_help())
    sys.exit()
  ros_farm = generate_ros_farm(None, csvfile, pattern=options.pattern, hosts_as_strings=True)

  # substitute all host references with the ones from the file
  reused_ros = False
  if options.subst_hosts:
    with open(options.subst_hosts, 'r') as pclist:
      for sd in ros_farm:
        for ros in ros_farm[sd]['ros']:
          assert isinstance(ros[0], list), "Probably some bug in the script!"
          nextline = '#'
          # skip commented
          while nextline.startswith('#'):
            try:
              nextline = next(pclist)
            except StopIteration:
              # EOF, recycle hosts
              reused_ros = True
              pclist.seek(0)
          nextline = nextline.strip()
          ros[0][0] = nextline
  printfarm(ros_farm)
  if reused_ros: print("# WARNING: pclist had too few hosts, had to recycle!")



