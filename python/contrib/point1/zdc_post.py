#!/usr/bin/env tdaq_python

# ZDC post processing script
# fixes only minor naming issues

from __future__ import print_function
def modify(segment):
    seg = segment[0]
    det = seg.get("Detector")[0]
    ros = seg.get("ROS")
    res = seg.get("ResourceSetAND")[0]

    if det.id!='FWD-ZDC':
      print("ZDC post processing script says: I was going"\
          " to rename FWD-ZDC to ZDC, but apparently no detector object"\
          " exists with this name. I won't rename the detector.")
    else:
      det.id = "ZDC_detector"
      for r in ros:
        r.Detector = det

    return segment
