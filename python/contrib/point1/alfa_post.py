#!/usr/bin/env tdaq_python

# ALFA post processing script
# fixes only minor naming issues

from __future__ import print_function
def modify(segment):
    seg = segment[0]
    det = seg.get("Detector")[0]
    ros = seg.get("ROS")
    res = seg.get("ResourceSetAND")[0]

    if det.id!='FWD-ALFA':
      print("ALFA post processing script says: I was going"\
          " to rename FWD-ALFA to ALFA_detector, but apparently no detector object"\
          " exists with this name. I won't rename the detector.")
    else:
      det.id = "ALFA_detector"
      for r in ros:
        r.Detector = det

    return segment
