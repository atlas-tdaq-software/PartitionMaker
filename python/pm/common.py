"""Methods for creating objects that are common to various segment types.

This module contains methods that simplify the creation of common objects that
are required in segments of different types. Examples are RDB servers, IS
servers and DFApplicationConfig objects.
"""
from __future__ import print_function

from builtins import str
from builtins import range
dbg = False

import logging

# The centralized TDAQ software repository
import pm.project
tdaqRepository = pm.project.Project('daq/sw/repository.data.xml')

from pm.dal import SFOngdal, HLTSVdal, HLTMPPUdal, PUDUMMYdal, DFdal, dal

# returns an oks dal.Computer for the local node
import pm.farm
localhost = pm.farm.local_computer()
pm.farm.remap_hwtag_tdaq([localhost])

def create_multicast_address(protocol="tcp", addmask=None):
  """Generates a random multicast address.

  This method generates a multicast address from its input parameters. If you
  decide to use 'tcp' as protocol (the default), the address generated becomes
  unimportant. Only switch to 'udp' if you are sure not to have two ROSs or L2
  result handlers on the same box!

  Keyword arguments:

  protocol -- This is the protocol that is going to be used (if you don't know
  what to choose, leave the default that is 'tcp').

  addmask -- This is the address/mask setting to have in the address field. If
  you don't specify the mask part, or set this option to None, no mask will be
  generated and the multicast message will be sent through a network card
  chosen by the kernel (read it to see which one). If this is set, the current
  message passing implementation will inforce the choice of network based on
  the mask you choose. The multicast address itself should be in the range
  (224.100.X.Y) where X is any number between 0 and 255 inclusive and Y,
  between 1 and 255 (inclusive). Different partitions running in the same
  network should have different addresses or they will interfere. As a remark,
  the multicast address picked up by this parameter will be only used by the
  dataflow message passing system if you also choose 'udp' as the protocol.
  """

  if protocol == 'tcp':
    return ''

  from random import seed, randint
  from time import time
  from string import join
  import socket
  seed(time())
  if not addmask:
    netmask = join(socket.gethostbyname(socket.gethostname()).split('.')[0:2],'.') + '.0.0'
    addmask = '224.100.%d.%d/%s' % (randint(0,255), randint(1,255), netmask)
  return (addmask)



def create_rdb_server(prefix, computer):
  """Creates an RDBServer.

  This method will create the RDBServer environment (application and
  environment variable) to be used by segments.

  Keyword arguments (may be named):

  prefix -- The prefix added to the RDB Application ID and Name, in such a way
  that the id and name will be of the form <prefix>-rdbs, and the environment
  variable will be <prefix>-TDAQ_DB_NAME.

  computer -- The oks computer object containing the node information on where
  the application will run. It is an error if computer is not set. 

  Returns a tuple where the elements are the following:
   1. The RDB Server InfrastructureApplication object.
   2. The RDB Server environment variable.
  """
  if not computer:
    raise RuntimeError('Cannot set RDB server in a segment without a host to run')

  rdbsAppName = "%s-rdbs" % (prefix)
  par = "-s -p ${TDAQ_PARTITION} -a TDAQ_DB_NAME -m RDB"
  return dal.InfrastructureApplication(rdbsAppName, 
      InitTimeout = 30,
      Parameters = par, 
      RestartParameters = par, 
      Program = tdaqRepository.getObject('Binary', "rdb_server"),
      IfDies = 'Restart', 
      RunsOn = computer,
      SegmentProcEnvVarName='TDAQ_DB_NAME', 
      SegmentProcEnvVarValue='appId')

def add_rdb_server(segment, computer=None):
  """Adds an RDB server to a dal.Segment.
  
  Keyword arguments:
  
  segment -- This is the dal.Segment object you want to attach a new RDB server
  to.
  
  computer -- This is the computer node (dal.Computer object) that the RDB
  server will run at. If not specified (None is the default), then it will
  search the DefaultHost in the segment. If that is not specified, I'll
  silently not add any RDB server to this segment.

  This method does not return anything.
  """
  from pm.utils import safeInsert
  if not computer: computer = segment.DefaultHost
  if not computer: return
  
  rdb = create_rdb_server(segment.id, computer)
  safeInsert(segment, 'Infrastructure', [rdb])

def create_is_server(prefix, computer=None):
  """Creates an ISServer.

  This method will create an ISServer object to be used by segments.

  Keyword arguments (may be named):

  prefix -- The prefix added to the IS Application ID and Name, in such a way
  that the id and name will be of the form <prefix>-is  

  computer -- The oks computer object containing the node information on where
  the application will run. If no computer is set, the IS server will run on
  the default host of the segment it belongs.

  Returns the InfrastructureApplication that represents an IS server
  """
  appName = "%s-iss" % prefix
  par = "-s -p ${TDAQ_PARTITION} -n %s" % appName
  retval =  dal.InfrastructureApplication(appName, 
                                          IfExitsUnexpectedly = "Restart", 
                                          IfFailsToStart = "Error", 
                                          InitTimeout = 20, 
                                          RestartableDuringRun = True, 
                                          Parameters = par, 
                                          RestartParameters = par,
                                          #IfFailed = "Restart", 
                                          #StartAt = "Boot", 
                                          #StopAt = "Shutdown",
                                          Program = tdaqRepository.getObject('Binary', 'is_server'))
  retval.SegmentProcEnvVarValue = 'appId'
  retval.SegmentProcEnvVarName = 'TDAQ_IS_SERVER'
  if prefix.find('Histogram') == 0: 
    retval.SegmentProcEnvVarName = 'TDAQ_OH_SERVER'

  if computer is not None: retval.RunsOn = computer
  return retval

def add_is_server(segment, prefix, computer=None):
  """Adds an IS server into the dal.Segment object of your choice.

  Keyword arguments:

  segment -- This is the dal.Segment object you want the IS server inserted at.
  prefix -- This is the prefix that will be attached to the IS server you are
  building. The final name will look like "<prefix>-<segment.id>-iss".
  computer -- This is the computer where this application is supposed to run.
  If this parameter is not given (the default is None), the server will run on
  the segment's default host.

  This method does returns the name of the IS server attached to the segment.
  """
  from pm.utils import safeInsert
  name = prefix + '-' + segment.id
  server = create_is_server(name, computer)
  safeInsert(segment, 'Infrastructure', [server])
  return server 

def create_resource_update(name, delay=10, active_nodes="ALL", server="",
    prefix=""):
  """Creates a DC_ISResourceUpdate object.

  This method will create a DC_ISResourceUpdate for simple resources. Most of
  the parameters can be kept on their defaults, except for the name, that will
  be used to name the resource and as the "name" parameter inside it.

  Keyword parameters (may be named):
  name -- The resource name (it will be also used for its id)
  delay -- The update delay in seconds
  active_nodes -- The types of nodes in which this resource will be active.
  This parameter should be either a list or a string containing one (or more)
  of the following: L2SV, L2PU, ROS (works for ROSE and L2RH), DFM, SFI, SFO,
  PT. The special value ALL makes the resource available on ANY application
  type.
  server -- This is a string that matches the IS server where to report the
  resource. It can be either the full name of the IS server or a string that
  ends with "*", which will then cause the reporting system to search for the
  next closest IS server with the matching prefix considering the segment
  structure where the application is sitting. The default is "" and I
  suggest you keep it. This means that the default IS server in the
  is taken from the DCApplicationConfig object where this update object is
  included.
  """
  obj_name = "ISResourceUpdate-%s" % name
  if len(prefix): obj_name = prefix + '-' + obj_name
  return DFdal.DC_ISResourceUpdate(obj_name, 
      name=name, delay=delay, isServer=server, activeOnNodes = active_nodes)

def create_histogram_storage(name, is_server="*Histogramming"):
  """Creates an HistogramStorage object.

  This method will create an histogram storage object that works well under
  most circumstances.

  Keyword parameters (may be named)
  name -- The objec id to set
  is_server -- The name of the ISServer to use to upload the histograms of
  providers using this storage object. The default "*Histogramming" entry
  should cover most of the conditions.
  """
  return DFdal.HistogramStorage(name, ISServerName=is_server)

def create_histogram_provider(name, update_freq, storage):
  """Creates a HistogramProviderConfig object.

  This method will return a default HistogramProviderConfig that works well
  under most circumstances.

  Keyword parameters (may be named)
  
  name -- The object id to set
  
  update_freq -- The update frequency in seconds
  
  storage -- A list of HistogramStorage objects to use for the storage of these
  histograms.
  """
  return DFdal.HistogramProviderConfig(name, UseHistory=False,
      UpdateFrequency=update_freq, SummingMode='AdvancedMerge', 
      ResultContent='Sum', Storage=[storage], PublishProviderName=name)

def create_histogram_receiver(name, provider, storage):
  """Creates an HistogramReceiverConfig object, used by Gatherer Applications.

  This method returns a DFdal.HistogramReceiverConfig that can be used for
  configuring gathering applications.
  
  Keyword parameters:
  name -- The object id to set
  
  provider -- This is a regular expression that is used inside the IS subsystem
  to select the histograms that are going to be read by the gatherer. It is
  normally something like "L2PU.*" or "PT.*" or ".*" (for everything).
  
  storage -- A list of HistogramStorage objects to use as the storage to read
  the data from.
  """
  return DFdal.HistogramReceiverConfig(name, Histograms=".*", 
      Providers=provider, Storage=storage)

def add_gatherer(segment, top_is, provider_regexp, result_prefix, 
    providers=[], computer=None, suffix=''):
  """This installs gatherers in segments or partitions.

  This method installs gatherer applications in a dal.Partition object or a
  dal.Segment object. It automatically detects the gatherer applications it
  needs (LVL2/EF) based on the segment's contents. You should provide the name
  of the IS server linked to the segment, as there maybe many and this is
  difficult to trace.

  segment -- This is the dal.Segment object to update
  
  top_is -- The string identifier of the top-level histogramming server

  provider_regexp -- This is the regular expression to use for reaching the
  providers you want to gather.

  result_prefix -- This is the resulting prefix of the new gatherer provider.
  
  providers -- These are other histogram storages that will serve as
  providers for the current gatherer. If this list is empty, it is assumed this
  is a base level gathering (directly from L2PUs or PTs) and the results will
  be placed on the same server. Otherwise, the results are considered to be
  gathered up.   
  
  computer -- The computer where the gatherers that will do this work will run.

  suffix -- This is a "desambiguation" suffix, that you can use to transform
  the name of the generated histogramming storages so there are no clashes when
  seperate segments, for example, are generated against the same top IS
  histogram.

  Returns the output storage object, so you can daisy chain gathering levels
  without having to copy objects.
  """
  from pm.utils import safeInsert, uniq
  receiver = create_histogram_storage(top_is + '-Storage' + suffix, 
      is_server=top_is)
  if not providers: providers = [receiver]
  providers = uniq(providers) # so we don't count double
  gath = create_gatherer(result_prefix + segment.id + '-Gatherer', 
      provider_regexp, providers, receiver, 30, 
      result_prefix + segment.id, computer)
  safeInsert(segment, 'Resources', [gath]) 
  return receiver 

def create_gatherer(name, provider, input_storage, output_storage,
    update_frequency, publish_name, computer=None):  
  """Creates a gatherer application.
  
  This method returns an object of type GATHERERApplication that will add-up the
  histograms at the input server defined by the provider regular expression and
  will dump the gathering results at the output server.

  Keyword parameters:
  
  name -- The object id to set
  
  provider -- This is a regular expression that is used inside the IS subsystem
  to select the histograms that are going to be read by the gatherer. It is
  normally something like "L2PU.*" or "PT.*" or ".*" (for everything).
  
  input_storage -- This is the input DFdal.HistogramStorage that will
  be used as the input server, where the gatherer will collect the histograms.
  
  output_storage -- This is the output DFdal.HistogramStorage that the
  gatherer will use as an output, where it dumps the gathered histograms.

  update_frequency -- This is the amount, in seconds, between the time the
  gatherer will try gathering. Has to be long enough so the application has
  actually the time to gather something before it has to run again. A good
  default that has been used is 30 seconds.
  
  publish_name -- This is the name under which the gatherer will publish the
  histograms collected.
  
  computer -- If given, this should be the dal.Computer object where the
  gatherer application is supposed to run.
  """
  retval = DFdal.GATHERERApplication(name)
  setup_dfapp(retval, on_problems='Ignore', short_timeout=5, long_timeout=120)
  retval.Program = tdaqRepository.getObject('Binary', 'Gatherer')
  if computer: retval.RunsOn = computer
  retval.Parameters = "-n " + name
  retval.RestartParameters = retval.Parameters 

  # to avoid the Gatherer to use the IPC proxy
  ipc_fast = dal.Variable('TDAQ_IPC_CLIENT_INTERCEPTORS',
      Name='TDAQ_IPC_CLIENT_INTERCEPTORS', Value='ipcclicp')
  if retval.ProcessEnvironment: retval.ProcessEnvironment.append(ipc_fast)
  else: retval.ProcessEnvironment = [ipc_fast]

  # the configuration part
  config = DFdal.GATHERERConfiguration(name + '-Configuration')
  config.Input = \
      create_histogram_receiver(name + '-Receiver', provider, input_storage)
  config.Output = \
      create_histogram_provider(name + '-Provider', update_frequency, 
          output_storage)
  config.Output.PublishProviderName = name #reset to something smaller
  retval.GATHERERConfiguration = [config]
  return retval

def check_doubles(object):
  """This method will check for doubles in an OKS object and will report.

  This method will check for doubles and will print their names to the standard
  output if it happens.

  Parameter:

  object -- This is the dal object to be checked.
  """
  existing = {}
  for o in object.get_all():
    if o.id in existing:
      logging.warning('Found duplicated linked from object %s@%s with identifier %s@%s (other type is %s)' % \
              (object.id, object.__class__.__name__, o.id,
                o.__class__.__name__, o.__class__.__name__))
    else:
      existing[o.id] = o

def create_histogram_update(name, path, provider, active_nodes):
  """Creates a DC_HistogramTypeUpdate object.

  This method returns DC_HistogramTypeUpdate oks object, already configured.
  The only parameters you have to pass are the name of this object (also used
  as the id of the object), the path of histograms you are interested to stream
  out and a list (or single) HistogramProviderConfig objects that define the
  intervals and histogramming servers to use.

  Keyword paramters (may be named):
  name -- The resource name (it will be also used for its id)
  path -- The path of histograms that will be taken from the appplication and
  published on the determined IS server.
  provider -- The configuration of the histogram server and update frequency.
  active_nodes -- The types of nodes in which this resource will be active.
  This parameter should be either a list or a string containing one (or more)
  of the following: L2SV, L2PU, ROS (works for ROSE and L2RH), DFM, SFI, SFO,
  PT. The special value ALL makes the resource available on ANY application
  type.
  """
  return DFdal.DC_HistogramTypeUpdate(name, name=name, path=path,
      ref_HistogramProviderConfig=[provider], activeOnNodes = active_nodes)


def create_default_trigger_config(name,opt):
  if dbg:
    print('create_default_trigger_config:')
    print('  creating a default pudummy configuration')
  return create_default_pudummy_trigger_config(name,opt)

def create_default_pudummy_trigger_config(name,opt): 
  """Creates our default TriggerConfiguration object.
  """
  if dbg: 
    print('pm/common.py: create_default_pudummy_trigger_config(name)')
    print('name =',name)
    print('opt  =',opt)

  from pm.dal import PUDUMMYdal

  accessAnySubdetector = PUDUMMYdal.DataAccess("accessAnySubdetector",
                                               subDetector=0,
                                               nROBDistribution="F|TMath::Gaus(x,2,2)|1|5",
                                               comment="1-5 ROBS from any sub detector.")

  accessAnySubdetector_1 = PUDUMMYdal.DataAccess("accessAnySubdetector-1",
                                                 subDetector=0,
                                                 nROBDistribution="C|1",
                                                 comment="Access 1 ROB in any sub detector.")

  l2_calophy_tag_0 = PUDUMMYdal.DummyStreamTag("L2-CaloPhysics-Tag-0",probability=0.20)
  l2_debug_tag_0   = PUDUMMYdal.DummyStreamTag("L2-Debug-Tag-0",      probability=0.00)
  ef_physics_tag_0 = PUDUMMYdal.DummyStreamTag("EF-Physics-Tag-0",    probability=0.25)
  ef_debug_tag_0   = PUDUMMYdal.DummyStreamTag("EF-Debug-Tag-0",      probability=0.10)
  
  dummyCalStreamTag = PUDUMMYdal.DummyCalStreamTag("EF-Calibration-Tag-0",
                                                   name="EF-Calibration-Tag-0",
                                                   probability=0.1,
                                                   calibData=accessAnySubdetector)

  efStep = PUDUMMYdal.DummyStep("efStep",
                                burnTimeDistribution="C|500000",
                                physicsTag=ef_physics_tag_0,
                                debugTag=ef_debug_tag_0,
                                calibrationTag=dummyCalStreamTag)

  
  calo_alg1_robstep = PUDUMMYdal.DummyROBStep("calo_algorithm-1",
                                              burnTimeDistribution="F|TMath::Gaus(x,20000,20)|5000|500000",
                                              physicsTag=l2_calophy_tag_0,
                                              debugTag=l2_debug_tag_0,
                                              calibrationTag=None,
                                              roi=[accessAnySubdetector_1])

  hlt_dummy_steering = PUDUMMYdal.PuDummySteering("HLT-Dummy-Steering",
                                                  libraries=["pudummy"],
                                                  resultSizeDistribution="C|1000",
                                                  nbrPscErrors=0,
                                                  seed=0,
                                                  steps=[calo_alg1_robstep,efStep])

  # no no hlt_dummy_steering is not a TriggerConfiguration, it's an HLTImplementation
  
  triggerConfiguration     = dal.TriggerConfiguration(name)
  triggerConfiguration.l1  = create_l1_trigconf('L1TrigConf',opt)
  triggerConfiguration.hlt = hlt_dummy_steering
  if dbg:
    print('triggerConfiguration:\n',triggerConfiguration)

  return triggerConfiguration

def create_jobo_trigger_config(name,opt):

  hlt_jobo_impl = HLTMPPUdal.HLTImplementationJobOptions("HLTImplementationJobOptions-1")

  if 'libraries' in opt:       hlt_jobo_impl.libraries       = opt['libraries']
  if 'jobOptionsPath' in opt:  hlt_jobo_impl.jobOptionsPath  = opt['jobOptionsPath']
  if 'evtSel' in opt:          hlt_jobo_impl.evtSel          = opt['evtSel']
  if 'pythonSetupFile' in opt: hlt_jobo_impl.pythonSetupFile = opt['pythonSetupFile']
  if 'preCommand' in opt:      hlt_jobo_impl.preCommand      = opt['preCommand']
  if 'postCommand' in opt:     hlt_jobo_impl.postCommand     = opt['postCommand']
  if 'showInclude' in opt:     hlt_jobo_impl.showInclude     = opt['showInclude']
  if 'logLevel' in opt:        hlt_jobo_impl.logLevel        = opt['logLevel']
  if 'tracePattern' in opt:    hlt_jobo_impl.tracePattern    = opt['tracePattern']

  hlt_jobo_impl.HLTCommonParameters = HLTMPPUdal.HLTCommonParameters('HLTCommonParameters-1')
  hlt_jobo_impl.HLTCommonParameters.jobOptionsSvcType = 'TrigConf::JobOptionsSvc'

  if 'messageSvcType' in opt:
    hlt_jobo_impl.HLTCommonParameters.messageSvcType    = opt['messageSvcType']
  if 'jobOptionsSvcType' in opt:
    hlt_jobo_impl.HLTCommonParameters.jobOptionsSvcType = opt['jobOptionsSvcType']

  triggerConfiguration = dal.TriggerConfiguration(name)
  triggerConfiguration.l1 = create_l1_trigconf('L1TrigConf',opt)
  triggerConfiguration.hlt = hlt_jobo_impl

  return triggerConfiguration

def create_db_trigger_config(name,opt):

  hlt_db_impl =  HLTMPPUdal.HLTImplementationDB("HLTImplementationDB-1")
  hlt_db_impl.libraries += ['TrigConfigSvc']

  if 'libraries' in opt:                      hlt_db_impl.libraries      = opt['libraries']
  if 'hltPrescaleKey' in opt:                 hlt_db_impl.hltPrescaleKey = opt['hltPrescaleKey']
  if 'additionalConnectionParameters' in opt: 
    hlt_db_impl.additionalConnectionParameters = opt['additionalConnectionParameters']

  hlt_db_impl.HLTCommonParameters = HLTMPPUdal.HLTCommonParameters('HLTCommonParameters-1')
  hlt_db_impl.HLTCommonParameters.jobOptionsSvcType = 'TrigConf::JobOptionsSvc'

  if 'messageSvcType' in opt:
    hlt_db_impl.HLTCommonParameters.messageSvcType    = opt['messageSvcType']
  if 'jobOptionsSvcType' in opt:
    hlt_db_impl.HLTCommonParameters.jobOptionsSvcType = opt['jobOptionsSvcType']

  if dbg:
    print('hlt_db_impl =',hlt_db_impl)
    print(hlt_db_impl.HLTCommonParameters)

  triggerConfiguration     = dal.TriggerConfiguration(name)
  triggerConfiguration.l1  = create_l1_trigconf('L1TrigConf',opt)
  triggerConfiguration.hlt = hlt_db_impl
  UID = 'TriggerDB_REPR_CoralServer'
  if 'TriggerDBConnection_UID' in opt: UID = opt['TriggerDBConnection_UID']
  triggerConfiguration.TriggerDBConnection = create_trig_db_connection(UID,opt)
  triggerConfiguration.DBConnections = create_db_connections(opt)

  return triggerConfiguration

def create_dbpython_trigger_config(name,opt):

  hlt_dbpy_impl =  HLTMPPUdal.HLTImplementationDBPython("HLTImplementationDBPython-1")
  hlt_dbpy_impl.libraries += ['TrigConfigSvc']

  if 'libraries' in opt:                      hlt_dbpy_impl.libraries      = opt['libraries']
  if 'hltPrescaleKey' in opt:                 hlt_dbpy_impl.hltPrescaleKey = opt['hltPrescaleKey']
  if 'additionalConnectionParameters' in opt: 
    hlt_dbpy_impl.additionalConnectionParameters = opt['additionalConnectionParameters']
  if 'preCommand' in opt:                     hlt_dbpy_impl.preCommand     = opt['preCommand']
  if 'postCommand' in opt:                    hlt_dbpy_impl.postCommand    = opt['postCommand']

  hlt_dbpy_impl.HLTCommonParameters = HLTMPPUdal.HLTCommonParameters('HLTCommonParameters-1')
  hlt_dbpy_impl.HLTCommonParameters.jobOptionsSvcType = 'TrigConf::JobOptionsSvc'

  if 'messageSvcType' in opt:              
    hlt_dbpy_impl.HLTCommonParameters.messageSvcType    = opt['messageSvcType']
  if 'jobOptionsSvcType' in opt:              
    hlt_dbpy_impl.HLTCommonParameters.jobOptionsSvcType = opt['jobOptionsSvcType']

  if dbg:
    print('hlt_dbpy_impl =',hlt_dbpy_impl)
    print(hlt_dbpy_impl.HLTCommonParameters)

  triggerConfiguration     = dal.TriggerConfiguration(name)
  triggerConfiguration.l1  = create_l1_trigconf('L1TrigConf',opt)
  triggerConfiguration.hlt = hlt_dbpy_impl

  UID = 'TriggerDB_REPR_CoralServer'
  if 'TriggerDBConnection_UID' in opt: UID = opt['TriggerDBConnection_UID']
  triggerConfiguration.TriggerDBConnection = create_trig_db_connection(UID,opt)

  triggerConfiguration.DBConnections = create_db_connections(opt)

  return triggerConfiguration

def create_db_connections(opt):
  """create the list of ATLAS_COOL* DBConnections etc

  this may have to vary depending on usage inside/outside P1
  """
  try:
    templates=pm.project.Project('daq/segments/HLT-Common.data.xml')
  except RuntimeError as e:
    print('ERROR: ',e)
    print('       TriggerConfiguration.DBConnections will not be generated/added')
    return []


  dbconnections0 = []
  dbconnections0.append(templates.getObject("DBConnection","ATLASDD_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_CSC_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_DCS_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_GLOBAL_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_INDET_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_LAR_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_MDT_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_PIXEL_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_SCT_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_TILE_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLOFL_TRT_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_CALO_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_CSC_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_GLOBAL_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_INDET_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_LAR_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_MDT_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_MUONALIGN_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_MUON_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_PIXEL_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_RPC_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_SCT_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_TDAQ_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_TGC_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_TGC_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_TILE_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_TRIGGER_CORALSRV"))
  dbconnections0.append(templates.getObject("DBConnection","ATLAS_COOLONL_TRT_CORALSRV"))

  dbconnections = templates.getObject("DBConnection")
  
  diffs1 = [ dbc for dbc in dbconnections if dbc not in dbconnections0 ]
  diffs2 = [ dbc for dbc in dbconnections0 if dbc not in dbconnections ]

  if dbg:
    print('\nelements that are in dbconnections but not in dbconnections0 :')
    for d in diffs1: print(d.id)
    print('\nelements that are in dbconnections0 but not in dbconnections :')
    for d in diffs2: print(d.id)
    print()

  return dbconnections

def create_trig_db_connection(name,opt):
  db_connection = create_trig_db_connection_default(name)

  if 'SuperMasterKey' in opt:               db_connection.SuperMasterKey = opt['SuperMasterKey']
  if 'TriggerDBConnection_Server' in opt:   db_connection.Server   =  opt['TriggerDBConnection_Server']
  if 'TriggerDBConnection_Port' in opt:     db_connection.Port     =  opt['TriggerDBConnection_Port']
  if 'TriggerDBConnection_Name' in opt:     db_connection.Name     =  opt['TriggerDBConnection_Name']
  if 'TriggerDBConnection_Alias' in opt:    db_connection.Alias    =  opt['TriggerDBConnection_Alias']
  if 'TriggerDBConnection_User' in opt:     db_connection.User     =  opt['TriggerDBConnection_User']
  if 'TriggerDBConnection_Password' in opt: db_connection.Password =  opt['TriggerDBConnection_Password']
  if 'TriggerDBConnection_Type' in opt:     db_connection.Type     =  opt['TriggerDBConnection_Type']

  return db_connection

def create_trig_db_connection_default(name):

  db_connection = dal.TriggerDBConnection(name)
  db_connection.Server   = 'LOCAL_HOST'
  db_connection.Port     = '3320'
  db_connection.Name     = '&amp;amp;oracle://ATLAS_CONFIG/ATLAS_CONF_TRIGGER_REPR'
  db_connection.Alias    = 'TRIGGERDBREPR'
  db_connection.User     = ''
  db_connection.Password = ''
  db_connection.Type     = 'Coral'

  return db_connection

def create_l1_trigconf(name,opt):
  l1_trigconf = dal.L1TriggerConfiguration(name)
  if 'Lvl1PrescaleKey' in opt:       l1_trigconf.Lvl1PrescaleKey       = opt['Lvl1PrescaleKey']
  if 'Lvl1BunchGroupKey' in opt:     l1_trigconf.Lvl1BunchGroupKey     = opt['Lvl1BunchGroupKey']
  if 'ConfigureLvl1MenuFrom' in opt: l1_trigconf.ConfigureLvl1MenuFrom = opt['ConfigureLvl1MenuFrom']
  return l1_trigconf

def create_trigger_config(opt):
  hlt_impl = opt['implementation'].lower()
  if   hlt_impl == 'pudummy':    return create_default_pudummy_trigger_config("PuDummyTriggerConfig-1",opt)
  elif hlt_impl == 'joboptions': return create_jobo_trigger_config("JobOptionsTriggerConfig-1",opt)
  elif hlt_impl == 'db':         return create_db_trigger_config("DBTriggerConfig-1",opt)
  elif hlt_impl == 'dbpython':   return create_dbpython_trigger_config("DBPythonTriggerConfig-1",opt)
  else:
    print('ERROR: wrong argument to create_trigger_config =',hlt_impl,opt['implementation'])
    print('       valid choices are (all case insensitive):')
    print('       pudummy')
    print('       joboptions')
    print('       db')
    print('       dbpython (db plus python preCommand and postCommand)')
    import sys
    sys.exit(1)

def create_datafile(name, location):
  """Creates a single DFdal.DataFile object that can be used for preloading.
  """
  return DFdal.DataFile(name, FileName=location)

def create_datafiles(prefix, local_list):
  """Creates an OKS list of DFdal.DataFile's from the location list.

  This method will return an list object that represents a list of datafiles
  of which locations are defined in the (python) list given as the second
  parameter.

  Keyword parameters:

  prefix -- A common prefix that will be used in all DataFile objects. The
  final name of each object will have a decimal number attached to it.
  
  local_list -- A (python) list of datafile locations. Remember these have to
  be visible at least at the L2SV/HLTSV and ROS/ROSE machines.

  Returns an list object with the DataFile's in it.
  """
  retval = []
  for f in range(0, len(local_list)):
    retval.append(create_datafile(prefix + ('-%d' % f), local_list[f])) 
  return retval

def create_df_parameters(name, network_protocol="tcp", datafiles=[]):
  """Creates a DFdal.DFParameters object.

  Keyword parameters:

  name -- The name of this object in the OKS database
  
  network_protocol -- This is the network protocol to use for data collection
  messages. The default is to use "tcp". This variable can, optionally, get the
  value "udp".
  
  datafiles -- A list of DFdal.DataFile objects that should be preloaded in ROSs.
  Its not sufficient that this parameter is set to make the ROS to preload data. 
  The relevant parametrization has also to be adjusted in these components.
  
  Returns a DFdal.DFParameters object
  """
  retval = DFdal.DFParameters(name)
  retval.Name = name
  retval.BasePortEFD_SFI = 10000
  retval.BasePortEFD_SFO = 11000
  retval.UsesDataFiles = datafiles
  try:
    import os, ipaddress
    # Find the first IPv4 address with global scope, hope this is the
    # "main" interface to use for local communication
    local_ip = ipaddress.ip_network(str(os.popen("/usr/sbin/ip -4 -oneline addr | grep 'scope global'").readline().split()[3]),strict=False)
    retval.DefaultDataNetworks = [
      local_ip.with_netmask
    ]
  except Exception as ex:
    retval.DefaultDataNetworks = [
      "128.141.0.0/255.255.0.0",
      "128.142.0.0/255.255.0.0",
      "188.184.0.0/255.255.0.0",
      "188.185.0.0/255.255.0.0",
      "137.138.0.0/255.255.0.0" ]

  retval.MulticastAddress = create_multicast_address()
  return retval

def add_rate_counter(part, type, event, rate):
  """Adds a "standard" event counter to a partition.

  This function creates and adds a standardized event counter that is attached
  to the IS_InformationSources to a partition.

  Keyword parameters:

  part -- The partition object that will be updated.

  type -- This is the type of counter that is being set, values are one of the
  existing ZERO TO ONE entries in IS_InformationSources objects. Currently they
  are "LVL1, LVL2, EB, EF and Recording", but you may want to check that for
  updates. 

  If the keyword being set is not one of the above, this will work additively,
  adding a new counter to the "Others" place holder in that object.

  event -- This is the event counter IS resource to be used, in the format
  "ISServername.ISObject.ISAttribute"

  rate -- The same as "event", but for rate.

  Does not return anything, just increments or resets your partition with the
  desired counters.
  """
  if not part.IS_InformationSource:
    part.IS_InformationSource = \
      dal.IS_InformationSources(part.id + '-counters')

  counter = dal.IS_EventsAndRates(part.id + '-' + type.lower() + '-counter',
      EventCounter=event, Rate=rate, Description=type + ' Counter')

  if hasattr(part.IS_InformationSource, type): 
    setattr(part.IS_InformationSource, type, counter) 

  else:
    #just add the new counter
    if not isinstance(getattr(part.IS_InformationSource, 'Others'), list):
      setattr(part.IS_InformationSource, 'Others', [])
    getattr(part.IS_InformationSource, 'Others').append(counter)

def create_partition(name, segments, df_parameters, environment, parameters, online_segment, 
                     trigger_config=None, logroot='/tmp', workdir='/tmp/${TDAQ_PARTITION}',
                     tags = ['x86_64-el9-gcc13-opt', 'x86_64-el9-gcc13-dbg',
                             'aarch64-el9-gcc13-opt'
                             ],
                     disabled=[], default_host=None):
  """Creates a dal.Partition object with the given parameters.
  
  This method will create a dal.Partition object taking into consideration the
  parameters input.

  Keyword parameters:
  
  name -- This is the partition name, for the OKS database
  
  segments -- A list of segments you would like to associate to this partition
  
  df_parameters -- A DFdal.DFParameter object
  
  environment -- A Variable/VariableSet or a list of those that will be
  associated with the partition
  
  parameters -- A Variable/VariableSet or a list of those that will be
  associated with the partition
  
  online_segment -- This is a pointer to the OnlineSegment that you want to use
  to setup the base infrastructure of this partition. It is normally taken from
  the file "daq/segments/setup.data.xml".

  trigger_config -- This is an object representing the TriggerConfiguration to
  attach to this partition.
  
  tags -- A list of tag identifiers for the Tag objects which are going to be
  included in your partition. The first tag is taken as the default for the
  partition, if no explicit tag is available in the application objects. The
  strings will be searched in our standard software repository. Unexisting tags
  will be discarded and a warning message generated. So you can improve your
  code.

  disabled -- This is a list of resources you want disabled since the start of
  your partition

  default_host -- This is the default host for the partition. If not set, the
  local host will be used.

  Returns a dal.Partition object
  """

  if dbg:
    print('create_partition: name =',name)
    print('                  segments =',segments)
    print('                  df_parameters =',df_parameters)
    print('                  environment =',environment)
    print('                  parameters =',parameters)
    print('                  online_segment =',online_segment)
    print('                  trigger_config =',trigger_config)

  # create_partition(name, segments, df_parameters, environment, parameters, online_segment, 
  #                  trigger_config=None, logroot='/tmp', workdir='/tmp/${TDAQ_PARTITION}',
  #                  tags={...}, disabled=[], default_host=None)

  retval = dal.Partition(name)
  retval.RepositoryRoot = ""
  retval.IPCRef = "$(TDAQ_IPC_INIT_REF)"
  retval.DBPath = "$(TDAQ_DB_PATH)"
  retval.DBName = "$(TDAQ_DB_DATA)"
  retval.DBTechnology = "rdbconfig"
  retval.LogRoot = logroot # '/tmp'
  retval.WorkingDirectory = workdir # '/tmp/${TDAQ_PARTITION}'

  # adds our standard counters

  add_rate_counter(retval, 'LVL1',      'DF.HLTSV.Events.LVL1Events',      '')
  add_rate_counter(retval, 'HLT',       'DF.HLTSV.Events.ProcessedEvents', 'DF.HLTSV.Events.Rate')
  add_rate_counter(retval, 'Recording', 'DF_IS:HLT.SFO-1.Counters.Global.WritingTotalEvents', 'DF_IS:HLT.SFO-1.Counters.Global.WritingEventRate')

  retval.Segments = segments
  retval.OnlineInfrastructure = online_segment
  retval.DataFlowParameters = df_parameters
  if trigger_config: retval.TriggerConfiguration = trigger_config

  retval.DefaultTags = []
  for t in tags:
    try:
      oks_tag = tdaqRepository.getObject("Tag", t)
      retval.DefaultTags.append(oks_tag)
    except RuntimeError:
      logging.warning("The tag with name %s is not available in the TDAQ sw repository. Not using it." % t)
  retval.Disabled = disabled
  retval.ProcessEnvironment = [environment]
  retval.Parameters = [parameters]

  if not default_host: retval.DefaultHost = localhost
  else: retval.DefaultHost = default_host

  return retval

def save_database(filename, object, includes):
  """Saves objects in an OKS database file.

  This method saved the object list passed in the filename you designate.

  Keyword parameters:
  
  filename -- The name of the file you want to record data at
  
  object -- A single or a list of dal objects you want to record in that
  file.
  
  include -- A list of includes to attach to the database. This is handy if you
  just want to re-use part or all of a database that already exists somewhere
  in your TDAQ_DB_PATH. The filenames can be either in the current directory or
  made relative to the TDAQ_DB_PATH. Objects which were extracted from such a
  databases are not going to be recorded in this new file.
  """
  from pm.project import Project
  db = Project(filename, includes)
  db.addObjects(object)
  # the deletion of db should record the file, finally

def setup_dfapp(app, on_problems="Error", short_timeout=5, long_timeout=60):
  """Sets-up the base Application class parameters.

  This method will setup the base dal.Application class parameters like
  timeouts and others. It is very generic and will setup defaults. If you don't
  use any of the parameters.

  Keyword parameters:

  app -- The dal.Application or derived class object to setup.
  
  on_problems -- This defines what the controller should do in case the
  application goes into a different state than OK. All three attributes related
  will be set to the same value. The default ("Error") makes the system to go
  into an error state. Other options are "Ignore" and "Restart". 
  
  short_timeout -- This is the timeout, in seconds, the application should take
  for short actions (like "start" or "shutdown"). The default is 20 seconds.
  
  long_timeout -- This is the timeout, in seconds, the application should take
  for longer actions (like "stop" or "configure"). The default is 60 seconds.

  This method does not return anything.
  """
  app.IfExitsUnexpectedly = on_problems
  app.IfFailsToStart = on_problems
  #app.IfError = on_problems
  
  app.InitTimeout = 60
  app.ActionTimeout = long_timeout
  #app.ShortTimeout = short_timeout

  if not (isinstance(app, DFdal.ROS) or \
          #isinstance(app, DFdal.ROSE) or \
          isinstance(app, DFdal.RoIBApplication)):
    app.RestartableDuringRun = True

def set_all(partition, class_name, attribute, value):
  """Sets attributes of all instances of objects in a partition.

  This method will blindly reset the attribute in the objects of type
  'class_name' to the given value.

  Keyword parameters:
  
  partition -- This the dal.Partition object to change. The method will
  navigate through all the linked objects.
  
  class_name -- The name of the dal class of which attribute you want set.
  
  attribute -- The name of the attribute in the dal class you want set.
  
  value -- This is the value you want set for the attribute
  """
  for k in partition.get(class_name): setattr(k, attribute, value)
 
def set_dummy_data(partition, value):
  """Sets the dummy size on the relevant parts of the partition.

  This method can operate pretty blindly and works ok for most configurations.

  Keyword parameters:

  partition -- This the dal.Partition object to change. The method will
  navigate through all the linked objects.

  value -- This is the value you want set for the dummy data size.
  """
  set_all(partition, 'ROSEConfiguration', 'robSizeDistribution', 'C|%d' % value)
  set_all(partition, 'MemoryPool', 'PageSize', value)


def load(filename):
  """Loads the OKS databases and returns a python dictionary for those.

  This method will load the OKS XML databases described in the input parameter
  (python list) and returns a dictionary that contains as keys, the names of
  each object found and as values their dal representations.
  """
  from pm.project import Project
  retval = {}
  for f in filename:
    for obj in Project(f).getAllObjects(): retval[obj.id] = obj
  return retval

def add_db_proxy(f, templates, proxy):
  """Place DB proxies into partition/segment

  full top-rack-node proxy here
  """

  from pm.utils import safeInsert

  if not proxy:
    return f

  top_proxy  = templates.getObject('InfrastructureTemplateApplication','TopCoralProxy')
  rack_proxy = templates.getObject('InfrastructureTemplateApplication','RackCoralProxy')
  node_proxy = templates.getObject('CustomLifetimeTemplateApplication','NodeCoralProxy')
  hlt_db_gen = templates.getObject('CustomLifetimeTemplateApplication','HLT_DB_Gen')

  #dbg = True # local override

  if dbg:
    print('add_db_proxy:')
    print('f                =',f)
    print('templates        =',templates)
    print('proxy            =',proxy)
    print('type(proxy)      =',type(proxy))

  if isinstance(proxy,type(True)):
    proxy = 'node'
    if dbg: print("proxy was True, now set to 'node'")
  elif isinstance(proxy,type(' ')):
    proxy0 = proxy
    proxy = proxy.lower()
    if proxy == 'true': proxy = 'node'
    if dbg: print("proxy was a string %s, now set to %s" % (proxy0,proxy))
  else:
    proxy = 'node'
    if dbg: print('all other cases: proxy set to %s' % proxy)

  if dbg:
    print('proxy after lower =',proxy)
    print('type(proxy)       =',type(proxy))

  # now we always need a top proxy
  # it must go into the Infrastructure of Segment HLT
  
  if isinstance(f,type([])):
    segs = f
  else:
    segs = f.Segments

  for s in segs:
    if s.id == 'HLT':
      safeInsert(s,'Infrastructure',[top_proxy])
      # rack proxies
      tsegs = s.Segments
      for ts in tsegs:
        if ts.id == 'HLT-1':
          if proxy == 'rack' or proxy == 'node':
            safeInsert(ts,'Infrastructure',[rack_proxy])
          # node proxies & hlt_db_gen
          if proxy == 'rack' or proxy == 'top':
            safeInsert(ts,'Applications',[hlt_db_gen])
            break
          if proxy == 'node':
            safeInsert(ts,'Applications',[node_proxy,hlt_db_gen])
            break
      break
  
  return f

def create_master_trigger(project):

  from pm.dal import dal, DFdal

  master_trigger = None
  hltsvs = project.getObject('HLTSVApplication')
  if len(hltsvs) == 0:
    print('create_master_trigger(project):\nNo HLTSVApplication found in project ',project)
    return master_trigger

  try:
    hltseg = project.getObject('Segment','HLT')
  except RuntimeError:
    print('create_master_trigger(project):\nCould not find Segment HLT')
    return master_trigger
  
  if hltseg:
    mt_name = 'HLTSVMaster'
    if len(hltsvs) == 1: # a single HLTSV, use it as master trigger
      controller = hltsvs[0]
    else:               # multiple HLTSVs, use rc_ctp_emulator as master trigger
      mt_name = 'CTPMaster'
      controller = dal.RunControlApplication('ctp')
      controller.Program = project.getObject('Binary','rc_ctp_emulator')
      project.updateObjects([controller])
      hltseg.Applications.append(controller)
      project.updateObjects([hltseg])

  master_trigger = dal.MasterTrigger(mt_name)
  master_trigger.Controller = controller
  
  return master_trigger
