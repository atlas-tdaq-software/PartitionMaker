#!/usr/bin/env tdaq_python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 10-May-2006

import pm.option.common as common
import pm.option.specific as specific

import copy
option = copy.deepcopy(common.option)

# This is a list of specific options that need to be taken from the common pool
# you can look them up over there to see their meaning
wanted = ['use-ros',
          'l2pus-per-node',
          'max-events',
          'l2pu-worker',
          'l2-hlt',
          'partition-name',
          'l2-farm',
          'ros-farm']

for k in wanted: option[k] = copy.deepcopy(specific.option[k])

def gendb(**kwargs):
  """Generates LVL2 only partition.
  
  Builds a partition that runs a LVL2 only system with L2SVs, L2PU's and
  ROS(E)s. The partition can be easily altered to work both in dummy or
  preloaded mode, with dummy algorithms or with the HLT software.

  Returns a dictionary that contains, as keys, the 'partition' (dal.Partition
  object) and the 'includes' python list of includes necessary to make the
  partition a valid OKS database.
  """
  import pm.common 
  import pm.utils
  from pm.project import Project
  from pm.multinode import part_l2, includes
  import re

  # and the farms
  ns = {}
  exec('from %s import l2_farm' % kwargs['l2-farm'], ns)
  exec('from %s import ros_farm' % kwargs['ros-farm'], ns)
  l2_farm  = ns['l2_farm']
  ros_farm = ns['ros_farm']

  datafiles = []
  if isinstance(kwargs['data'], list):
    datafiles = pm.utils.unfold_patterns(kwargs['data'])

  part = part_l2(kwargs['partition-name'], l2_farm, ros_farm, 
      setup=Project(kwargs['setup-file']),
      l2pu_instances=kwargs['l2pus-per-node'],
      emulator=(not kwargs['use-ros']),
      datafiles=datafiles)

  # sets the RepositoryRoot at the partition level
  part.RepositoryRoot = kwargs['repository-root']

  # sets the L2SV configuration to a maximum number of events
  pm.common.set_all(part, 'L2SVConfiguration', 'eventMax', kwargs['max-events'])

  # sets the datasize of the different fields.
  if not isinstance(kwargs['data'], list):
    pm.common.set_dummy_data(part, int(kwargs['data']))

  # sets the number of l2pu workers
  pm.common.set_all(part, 'L2PUConfiguration', 'noWorkerThreads', 
      kwargs['l2pu-worker'])

  pm.common.set_all(part, re.compile('^L2PU.*Application$'), 
      'TriggerConfiguration',
      pm.common.create_trigger_config(kwargs['l2-hlt'], None))

  # saves the new partition on a new database
  includes.append(kwargs['setup-file']) 
  
  return {'partition': part, 'includes': includes}
