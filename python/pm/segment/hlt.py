#!/usr/bin/env tdaq_python
# Created by Haimo Zobernig <Haimo.Zobernig@cern.ch>, 29-Apr-2013

import pm.option.segcommon as common
import pm.option.specific as specific

import copy
import sys

option = copy.deepcopy(common.option)

dbg = False

# This is a list of specific options that need to be taken from the utils
# you can look them up over there to see their meaning

wanted = ['data-networks',
          'hltpus-per-node',
          'hltpu-numForks',
          'hltpu-numberOfAthenaMTThreads',
          'hltpu-numberOfEventSlots',
          'write-dir',
          'max-events',
          'hlt-implementation',
          'hlt-extra-path',
          'hlt-farm',
          'hlt-dqm',
          'proxy',
          'muon-calibration',
          ]


for k in wanted: option[k] = copy.deepcopy(specific.option[k])

def gendb(**kwargs):
  """Generates an HLT segment you can attach to existing partitions.

  This method generates a multinode HLT segment that can be attached to
  existing partitions.

  Returns a dictionary that contains, as keys, the 'segment' (dal.Segment
  object) and the 'includes' python list of includes necessary to make the
  segment a valid OKS database.  
  """
  import pm.common 
  import pm.utils
  import pm.multinode
  from pm.project import Project

  # my list of includes
  includes = list(pm.multinode.includes)

  if dbg:
    print('\npm/segment/hlt.py:\n')
    print('includes:\n',includes)

  # and the farms
  ns = {}
  exec('from %s import hlt_farm as hlt_farms' % kwargs['hlt-farm'], ns)
  hlt_farms = ns['hlt_farms']
  includes = pm.utils.merge_includes(includes, kwargs['hlt-farm'], 'includes')

  # do we need output on a file
  output = []
  if kwargs['write-dir'] and len(kwargs['write-dir']) != 0: 
    output = kwargs['write-dir']

  #obj_cache = pm.common.load(kwargs['extra-includes'])

  segs = []
  if not isinstance(hlt_farms, list): hlt_farms = [hlt_farms]

  try:
    hlt_impl = kwargs['hlt-implementation']['implementation']
  except KeyError:
    print("ERROR, option['hlt-implementation'] =",kwargs['hlt-implementation'])
    sys.exit(1)

  real_hlt = False
  if str.lower(hlt_impl) != 'pudummy':
    #
    # set up the HLT environment as far as needed by PM
    #
    hlt_env = pm.utils.hltEnv()
    hlt_env.set_hlt_env(kwargs['hlt-implementation'])
    #
    # add some HLT includes
    #
    includes.append('daq/sw/HLT_SW_Repository.data.xml')
    #
    real_hlt = hlt_env

    if dbg:
      print('includes:')
      for i in includes: 
        print(i)


  for hlt_farm in hlt_farms:
    seg = pm.multinode.gen_hlt_segment(Project(kwargs['template-file']), 
                                       hlt_farm,
                                       hltpu_instances=kwargs['hltpus-per-node'], 
                                       hltpu_numForks=kwargs['hltpu-numForks'], 
                                       hltpu_numberOfAthenaMTThreads=kwargs['hltpu-numberOfAthenaMTThreads'],
                                       hltpu_numberOfEventSlots=kwargs['hltpu-numberOfEventSlots'],
                                       hlt_patch_area=kwargs['hlt-extra-path'],
                                       output=output,
                                       real_hlt=real_hlt)
    segs.append(seg)
  
    # sets the datasize of the different fields. 
    if not isinstance(kwargs['data'], list):
      pm.common.set_dummy_data(seg, int(kwargs['data']))
      

    # special goodies
    import pm.special
    if kwargs['control-only']: pm.special.empty_control_tree(seg)
    #if kwargs['no-templates']: pm.special.untemplatize(seg)
    if kwargs['hlt-dqm']: 
      pm.special.add_seg(seg, kwargs['hlt-dqm'])
      includes.append(kwargs['hlt-dqm'])

    hlt_netmask = kwargs.get('hlt-datanet-mask', None)
    datanetworks = kwargs.get('data-networks', None)
    pm.special.reset_mesg_passing(seg, datanetworks, hlt_netmask) 

  # saves the new partition on a new database
  includes = pm.utils.merge_unique(includes, kwargs['template-file'])

  if kwargs['muon-calibration']:
    import pm.mucal
    # no need to append the created segment as it is built into the currently
    # existing infrastructure
    pm.mucal.create_top_level_segment('MUCal-' + seg.id, 
                                      pm.project.Project(kwargs['muon-calibration']), seg)
    includes.append(kwargs['muon-calibration'])


  if len(kwargs['extra-includes']) != 0:
    includes = pm.utils.merge_unique(includes, kwargs['extra-includes'])

  return {'segment': segs, 'includes': includes}
