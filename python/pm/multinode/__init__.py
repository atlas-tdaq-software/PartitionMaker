from __future__ import absolute_import
from builtins import str
import os

from .GLOBALS import *

from .ROS import *
from .HLT import *
from .PARTITIONS import *

hlt_exports   = ['hlt_subfarm', 'sub_rack_farm', 'hlt_farm','gen_hlt_segment']
part_exports  = ['part_hlt']
ros_exports   = ['ros_farm_subdet','ros_farm_random','gen_ros_segment','gen_reb_segment']
#roib_exports  = ['roib_farm','gen_roib_segment']
other_exports = ['includes','global_counter','prettyprint']

__all__ = ros_exports + hlt_exports + part_exports + other_exports


def prettyprint(obj, indent=''):
  """Recursively prints the farm contents in a readable way.
  
     obj    -- the object to be prettyprinted
     indent -- extra indentation string
  """

  from config.dal import DalBase

  retval = ''
  global_counter['PP_REC_LEVEL'] += 1
  
  if type(obj) is dict:
    retval += '{\n'
    for k, v in list(obj.items()):
      retval += '%s  %s: %s' % (indent, k, prettyprint(v, indent + '  '))
      retval += ',\n'
    retval += indent + '}'
  elif type(obj) is list:
    copy = list(obj)
    copy.sort()
    retval += '[\n'
    for k in copy:
      retval += '%s  %s' % (indent, prettyprint(k, indent + '  '))
      retval += ',\n'
    retval += indent + ']'
  elif type(obj) is tuple:
    retval += '(\n'
    for k in obj:
      retval += '%s  %s' % (indent, prettyprint(k, indent + '  '))
      retval += ',\n'
    retval += indent + ')'
  elif isinstance(obj, DalBase):
    retval += obj.fullName()
  elif isinstance(obj, int):
    retval += '0x%08x' % obj
  else:
    retval += str(obj)

  global_counter['PP_REC_LEVEL'] -= 1
  
  return retval
