"""Helps users on their interation with the "config" package

This module includes the description of the Project class, which acts
like a wrapper to the OKS configuration system, providing the user a
simpler interface to build its own databases. It accomplishes this
by converting ConfigObjects to their pythonic DAL representation and
helping the management of include files.
"""

import config
import re
import os
import logging

import pm.utils

class Project (config.Configuration):
  """This is the base class for OKS databases, partitions and segments.

  Objects of this class function as a wrapper around the 'config' package
  primitives (i.e., Configuration and ConfigObjects) providing the user a
  simpler interface to add, get, read and write OKS configuration files. It
  uses the functionality built into config packages to transform ConfigObjects
  into DAL equivalents and vice-versa in a transparent way.

  Objects of this class can also manage include files that should be added to
  the final generated database.
  """

  def __init__(self, *args):
    """Initializes a new project.

    Arguments:

    There are two modes of operation for this constructor. You can either load
    an existing OKS database or create a new one.

    To load an existing database, use a zero or a single parameter construction:

    [0] -- The database string you want to load. This follows the
    ${TDAQ_DB} variable convention you find the TDAQ release. If the value of
    tdaq_db does not contain a ':' separator, the DB backend to be loaded
    will default to "oksconfig".

    Examples:
    proj = Project("mysegment.data.xml")
    proj = Project("rdbconfig:test-partition")
    proj = Project()

    If the value of first parameter is not set, the system will try to use the
    value of the environment variable TDAQ_DB to load the database. This can
    also work from python like this:

    import os; os.environ['TDAQ_DB'] = 'oksconfig:part.data.xml'
    proj = Project()

    Once your Project has been initialized, you can use methods like
    addInclude(), removeInclude() or getIncludes() to add, remove and list the
    include file syour have added to the database. These methods are inherited
    from the config.Configuration parentship.

    To create a new database, use a constructor with two parameters, where the
    second parameter is a list of possible includes to the database. If you
    want no includes, specify an empty list.

    [0] -- The name of the new database to create
    [1] -- A single or a list of strings specifying the include files to be
    used in the newly created database. Optionally, this can be an empty list.

    Examples:
    proj = Project("mynewdb.data.xml", [])
    proj = Project("mydb.data.xml", ['includeA.data.xml', 'includeB.data.xml'])
    """
    if len(args) == 0: super(Project, self).__init__()
    elif len(args) == 1:
      if isinstance(args[0], str):
        if args[0].find(':') != -1: super(Project, self).__init__(args[0])
        else: super(Project, self).__init__('oksconfig:' + args[0])
      else:
        # initialized from the DB editor, just pass on
        super(Project, self).__init__(args[0])
    elif len(args) == 2:
      super(Project, self).__init__()
      self.create_db(args[0], args[1])
    else:
      raise SyntaxError("Unsupported number of constructor parameters (" + \
          len(args) + ")")

  def getIncludes(self, at=None):
    """Returns a a list of all includes in a certain database.

    Keyword arguments:

    at -- This is the name of the database you want to get the includes from.
    If set to 'None' (the default) we use whatever self.active_database is set
    to hoping for the best.
    """
    return self.get_includes(at)

  def removeInclude(self, include, at=None):
    """Removes a included file in a certain database.

    This method will remove include files from the active database or from any
    other include database if mentioned.

    WARNING: After using this method, please call pm.project.Project.commit()
    to commit your changes as this method will *not* do it because of transient
    consistency issues that are potentially triggered by adding and removing
    include databases.

    Keyword parameters:

    include -- This is either a single include or a list of includes to remove
    from the database.

    at -- This is the name of database you want to remove the include(s) from,
    If set to None (the default), I'll simply use the value of
    'self.active_database', hoping for the best.
    """
    if not isinstance(include, list): include = [include]
    for i in include: self.remove_include(i, at)

  def addInclude(self, include, at=None):
    """Adds a new include to the database.

    This method includes new files in the include section of your database. You
    can specify the file to which you want to add the include file. If you
    don't, it uses the last opened (active) file.

    WARNING: After using this method, please call pm.project.Project.commit()
    to commit your changes as this method will *not* do it because of transient
    consistency issues that are potentially triggered by adding and removing
    include databases.

    Keyword parameters:

    include -- This is either a single or a list of includes to add into your
    database

    at -- This is the name of database you want to add the include at, If set
    to None (the default), I'll simply use the value of 'self.active_database',
    hoping for the best.
    """
    if not isinstance(include, list): include = [include]
    for i in include: self.add_include(i, at)

  def addObjects(self, objs, recurse=True):
    """Adds OKS objects to the current opened database.

    This method includes one or more DAL objects to the opened database. If the
    object already exists within the project, it will *not* be recreated, but
    reused. After the new additions, the database is commited.

    Keyword Arguments (no name specified):

    objs -- A python list of (pythonic) DAL objects.

    recurse -- A boolean flag that determines if I should go adding recursively
    (the default) or not, in which case I'll just try to add the current list
    of objects.
    """
    for obj in objs:
      self.add_dal(obj, recurse=recurse)
    self.commit()

  def updateObjects(self, objs, recurse=False):
    """Adds and updates OKS objects at the current opened database.

    This method includes one or more DAL objects to the opened database. If the
    object already exists within the project, the existing one will be replaced
    by the one being included. After the inclusions, the database is commited.

    Keyword Arguments (no name specified):

    objs -- A python list of (pythonic) DAL objects.

    recurse -- A boolean flag that determines if I should go updating
    recursively or not (the default), in which case I'll just try to add the
    current list of objects.
    """
    for obj in objs: self.update_dal(obj, ignore_error=True, recurse=recurse)
    self.commit()

  def removeObjects(self, objs, depth=0):
    """Removes OKS objects from the current opened database.

    This method will remove objects from the current opened database. For an
    object to be removed from the database, you need to make sure to unlink it
    from all of its references before. Currently, it is not possible to update
    references for removed objects.

    Keyword parameters:

    objs -- The DAL objects (in a list) you want to remove from this project

    depth -- The depth to which I should proceed. This parameter should not be
    set and will be removed in the next release.
    """
    if depth != 0:
      logging.warning('The depth parameter has no effect, it became obsolete.')
    for k in objs: self.destroy_dal(k)
    self.commit()

  def getObject(self, className, idName=None):
    """Returns the desired object based on its class and ID values.

    Searches the currently opened database and included files, returning the
    desired object based on its class and ID values.

    Keyword arguments (may be named):
    className -- The name of the object's class.

    idName    -- The object's ID name. If this value is set to None (this is
    the default), then all objects from the specified class are returned.

    Returns the object(s) matching the criteria. If the idName is specified,
    then the return value is the object, otherwise, it is a list containing the
    objects belonging to the specified clas found.
    """
    if idName: return self.get_dal(className, idName)
    else: return self.get_dals(className)

  def getAllObjects(self, regexp=re.compile('.+')):
    """Returns all objects which match the regexp given in the current DB.

    This method will return, all objects in the current database that have a
    class matching the regular expression given as parameter. By default this
    expression is set so all objects in the database are returned.

    Keyword parameters:

    regexp -- A regular expression to filter the existing python DAL. This will
    limit the search of classes (this parameter is currently *not* used).

    """
    return list(self.get_all_dals().values())

def addDAL(module):
  """Adds new classes to the Project class known DAL.

  Keyword parameters:

  module -- This is the python module that will have its DAL classes added to
  the know DAL.
  """
  mesg = """This function will be outdated in the next major TDAQ release. You no longer need to add DALs to the running code, so you may delete this call.  The sole exception is if you actively create objects from a DAL in your scripts. In this case, you should bind the relevant DALs just before using their functionality. You can follow the recipes in the PartitionMaker twiki:
  https://twiki.cern.ch/twiki/bin/view/Atlas/PartitionMaker
  """
  logging.warning(mesg)
