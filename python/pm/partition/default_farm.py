#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Mon 10 Sep 2007 06:59:03 PM CEST 

"""Defines the default farm structure to be used in multinode partitions.

This module defines a dummy, default farm structure to be used in multinode
partitions. The definition of the farm objects can be found in the several
gen_* functions in the pm.multinode submodule.

For obvious reasons, all components will be put on the local computer.
"""
from __future__ import absolute_import

import pm.multinode
from pm.multinode import localhost # re-binds locally

# prepares the HLT subsystem
hlt_subfarm = pm.multinode.hlt_subfarm([localhost])
hlt_farm = pm.multinode.hlt_farm([localhost],[hlt_subfarm],sfo=[localhost])

# prepares the ROS subsystem
from . import default_robhit
ros_farm = pm.multinode.ros_farm_random(default_robhit.robhit, [localhost])


