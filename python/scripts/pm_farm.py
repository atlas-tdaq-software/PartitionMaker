#!/usr/bin/env tdaq_python
# Andre Anjos <Andre.dos.Anjos@cern.ch

# Manipulates farmscanner clusters
from __future__ import print_function
import sys
import os
from EventApps import myopt
import re
import logging

from pm.dal import dal

app_name = os.path.basename(sys.argv[0])

help_msg = """Maintains OKS databases of hardware clusters. 

The general command format is:
$ %s <command> <machine-list or regular-expression> <oks-database-file>

The machine list can be composed of expandable patterns that can take one
of the formats bellow:
  1. Normal machine name, e.g., pc-preseries-l2p-01 or, example.cern.ch
  2. A range, e.g., pcatb[120-150]
  3. A set of ranges, e.g., pcatb[120-130, 135-150].cern.ch

Please read the documentation in pm.pattern to understand how to best adapt the
pattern expansion to your needs.

This program will also set the network interface labels if the user specifies
so.

Examples:
  # This example generates the hardware XML database for all the nodes
  # which are reacheable within the specified range of computers. All
  # network interfaces with IP address starting with "192.168.100" will
  # be set to act as L2 (DC1) data network. Avoid the spaces in the tuples.
  %s --safe-hw-tags --add='pcatb[120-150]' --net-label-name='("DC1","eth1")' pcatb_cluster.data.xml

  # This example does the same as above, but shows how to setup different
  # networks for different machines. Avoid the spaces in the tuples.
  %s --safe-hw-tags --add='pc-preseries-l2p-[01-12]' --add='pc-preseries-efp-[01-30]' --net-label-ip='("DATA","192.168.200.+")' --net-label-ip='("DATA","192.168.150.+")'

  # Of course the example above and be re-written as:
  %s --safe-hw-tags --add='pc-preseries-l2p-[01-12]' --add='pc-preseries-efp-[01-30]' --net-label-ip='("DATA","192.168.(150|200).+")'

The script returns 0 (zero) on success""" % (app_name, app_name, app_name, app_name)

option = {}

# Lists all machines in a certain cluster
option['list'] = {'short': 'l', 'arg': False, 'default': None,
                  'group': 'Database manipulation',
                  'description': 'lists machines in a cluster'}

# Lists all machines in a certain cluster
option['list-matching'] = {'short': 'L', 'arg': True, 'default': '',
                           'group': 'Database manipulation',
                           'description': 'lists machines in a cluster that mach a (python) regular expression'}

# Lists all machines in a certain cluster
option['count'] = {'short': 'c', 'arg': False, 'default': None,
                  'group': 'Database manipulation',
                  'description': 'returns the number machines in the cluster file'}

# Lists all machines in a certain cluster
option['delete'] = {'short': 'd', 'arg': True, 'default': [],
                    'group': 'Database manipulation',
                    'description': 'remove one or more machines or sets from the cluster'}

# Lists all machines in a certain cluster
option['add'] = {'short': 'a', 'arg': True, 'default': [],
                 'group': 'Database manipulation',
                 'description': 'add or update one or more machines from the cluster'}

option['computer-set'] = {'short': 'e', 'arg': True, 'default': {},
                           'group': 'Database manipulation',
                           'description': 'Determines the organization of computers and sets in a HW database as a python dictionary (string keys and list values with machine names or patterns). Nesting of dictionaries is supported. Please note that this is a post-processing option. Nodes should still be added with the --add flag. Example: {"set1": ["host[1-5]","host32"], "set2": ["host[6-31]",{"subset2":["host[33-40]"]}]}'}
                          
option['rack'] = {'short': 'r', 'arg': True, 'default': {},
                  'group': 'Database manipulation',
                  'description': 'Determines the organization of computers and sets in a HW database in racks, here represented as a python dictionary (string keys and list values with machine names or patterns). Nesting of dictionaries is supported. Please note that this is a post-processing option. Nodes should still be added with the --add flag. Example: {"rack1": {"LFS":"lfs-01", "Nodes":["host[1-5]","host32","setA"]}, "rack2": ...}'}
                           
# Merges the node from another cluser file
option['merge'] = {'short': 'm', 'arg': True, 'default': '',
                   'group': 'Database manipulation',
                   'description': 'Name of another cluster file to be merged with the current file'}

option['safe-hw-tags'] = {'short': 'f', 'arg': False, 'default': None,
                          'group': 'Database manipulation',
                          'description': 'if this flag is set, the hardware tag choosen for each machine will be the best match of existing TDAQ release binaries'}

option['ignore'] = {'short': 'i', 'arg': False, 'default': None,
                    'group': 'Database manipulation',
                    'description': 'Should I still add unreacheable nodes?'}

option['net-label-ip'] = {'short': 'n', 'arg': True, 'default': [],
                          'group': 'Database manipulation',
                          'description': 'If set, this list should be composed of tuples, each containing two components: (the type of network, regular expression). This will cause the program to set the "Label" parameter on the matching networks taking as base the interface ip address (e.g. 192.168.100.+)'}

option['net-label-name'] = {'short': 'N', 'arg': True, 'default': [],
                            'group': 'Database manipulation',
                            'description': 'If set, this list should be composed of tuples, each containing two components: (the type of network, regular expression). This will cause the program to set the "Label" parameter on the matching networks taking as base the interface name (e.g. eth0)'}

option['use-short-names'] = {'short': 'Q', 'arg': False, 'default': None,
                             'group': 'Database manipulation',
                             'description': 'If set, all commands will refer to the machine names using their short, non-FQDN (i.e. without the domain). Please note that this only affects the way that PM interact with the nodes on your HW database and *not* how they are written out, since TDAQ requires HW databases to only use FQDN.'}

option['update'] = {'short': 'u', 'arg': False, 'default': None,
                    'group': 'Database manipulation',
                    'description': 'Updates all nodes in the database'}

# Defines the timeout period to give-up on a node answer 
option['timeout'] = {'short': 't', 'arg': True, 'default': 20,
                     'group': 'SSH',
                     'description': 'The ssh timeout in seconds'}

option['extra-includes'] = {'short': 'I', 'arg': True,
  'group': 'Database manipulation',
  'description': 'Extra OKS includes to your output',
  'default': []}

def unfold_patterns(l):
  """This function checks the list l for patterns and will unfold them for you
  making a full blown-up list of patterns and returning that.
  """
  from pm.pattern import expression
  from pm.utils import mkList, uniq

  l = mkList(l) #make sure we have got a list
  to_expand = []
  for k in l:
    if k.find('[') != -1: to_expand.append(k)

  for k in to_expand:
    l.remove(k)
    l.extend(expression([k]))

  l = uniq(l)
  return l

def print_recursive(l, prefix):
  """Prints ComputerSets recursively."""
  for k in l:
    if isinstance(k, dal.Computer): print(prefix + repr(k))
    elif isinstance(k, dal.ComputerSet):
      print(prefix + repr(k) + ':')
      print_recursive(k.Contains, prefix + '  ')

def make_sets_recursive(cluster, descr):
  """Adds ComputerSets recursively to the cluster, if they are not there."""
  from pm.utils import mkList
  
  retval = {} 
  for k,v in descr.items():
    newset = dal.ComputerSet(k, Contains=[])
    # add computers
    for entry in mkList(v):
      if not isinstance(entry, dict): #it is a node
        for i in unfold_patterns(entry):
          if i in list(cluster.keys()): newset.Contains.append(cluster[i])
      else: #it is a subset
        just_built = make_sets_recursive(cluster, entry)
        for nv in just_built.values(): newset.Contains.append(nv)
        cluster.update(just_built)
        retval.update(just_built)
    retval[k] = newset
  return retval

def make_racks(cluster, descr):
  """Adds Racks recursively to the cluster, if they are not there."""
  from pm.utils import mkList
  
  retval = {} 
  for k,v in descr.items():
    newrack = dal.Rack(k, Nodes=[])
    # add computers
    if 'LFS' in v:
      if v['LFS'] in cluster:
        newrack.LFS = mkList(cluster[v['LFS']])
      else:
        logging.warning('Ignoring setting of "LFS" entry for rack "%s" because the computer "%s" is not described' % (k, v['LFS']))
    if 'Nodes' in v:
      for entry in mkList(v['Nodes']):
        for i in unfold_patterns(entry):
          if i in cluster: newrack.Nodes.append(cluster[i])
    retval[k] = newrack
  return retval

def main():
  """The main entry point for this program"""
  from pm.utils import computer_list, merge_unique

  if len(sys.argv) < 2:
    parser = myopt.Parser(extra_args=True, post_options=help_msg)
    for (k,v) in list(option.items()):
      parser.add_option(k, v['short'], v['description'], v['arg'],
          v['default'], v['group'])
    print(parser.usage())
    sys.exit(1)

  # process the global options
  parser = myopt.Parser(extra_args=True, post_options=help_msg)
  for (k,v) in list(option.items()):
    parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])
  (kwargs, extra) = parser.parse(sys.argv[1:],
      prefix='"%s" options:' % sys.argv[0])

  # check for the extra argument
  if len(extra) != 1:
    print('There should be one and only one extra argument: the cluster file')
    print('I cannot accept:', extra)
    print(parser.usage('"%s" options:' % sys.argv[0]))
    sys.exit(1)

  # loads the hw database utilities
  import pm.farm

  if os.path.exists(extra[0]):
    # checks if we don't have extra includes from the main file
    import pm.project
    kwargs['extra-includes'] = merge_unique(pm.project.Project(extra[0]).getIncludes(), kwargs['extra-includes'])
    cluster = pm.farm.load(extra[0], short_names=kwargs['use-short-names'],
        includes=kwargs['extra-includes'])

  else:
    cluster = {}

  # does operations on the cluster
  changed = False

  if len(kwargs['delete']) != 0:
    for k in unfold_patterns(kwargs['delete']):
      if k in cluster: del cluster[k]
    changed = True

  if kwargs['update']:
    names = [k for k in list(cluster.keys()) if isinstance(cluster[k], dal.Computer)]
    data = pm.farm.network_computer(names, kwargs['ignore'], kwargs['timeout'])
    for k in data:
      key = k.id
      if kwargs['use-short-names']: key = key.split('.', 1)[0]
      cluster[key] = k
    changed = True

  if len(kwargs['add']) != 0:
    data = pm.farm.network_computer(kwargs['add'], kwargs['ignore'],
      kwargs['timeout'])
    for k in data:
      key = k.id
      if kwargs['use-short-names']: key = key.split('.', 1)[0]
      cluster[key] = k
    changed = True

  if len(kwargs['merge']) != 0:
    data = pm.farm.load(kwargs['merge'], short_names=kwargs['use-short-names'])
    for k, v in list(data.items()):
      if k in cluster:
        logging.warning("Overwriting info for computer %s" % cluster[k].id)
      cluster[k] = v
    changed = True

  if len(kwargs['net-label-ip']) != 0:
    for label, regexp in kwargs['net-label-ip']:
      m = [cluster[k] for k in list(cluster.keys()) \
          if isinstance(cluster[k], dal.Computer)]
      pm.farm.set_network_label(m, 'IPAddress', re.compile(regexp), label)
    changed = True

  if len(kwargs['net-label-name']) != 0:
    for label, regexp in kwargs['net-label-name']:
      pm.farm.set_network_label(list(cluster.values()), 'InterfaceName',
        re.compile(regexp), label)

  if kwargs['safe-hw-tags']:
    pm.farm.remap_hwtag_tdaq([k for k in list(cluster.values()) if \
        isinstance(k, dal.Computer)])
    changed = True

  # a list of externals to be used for making Racks and ComputerSets
  all = {}
  for i in kwargs['extra-includes']:
    all.update(pm.farm.load(i, short_names=kwargs['use-short-names']))
  all.update(cluster)

  if len(kwargs['computer-set']) != 0:
    changed = True
    cluster.update(make_sets_recursive(all, kwargs['computer-set']))
    all.update(cluster)

  if len(kwargs['rack']) != 0:
    changed = True
    cluster.update(make_racks(all, kwargs['rack']))
    all.update(cluster)

  if changed: 
    to_save = list(cluster.values())
    pm.farm.save(extra[0], to_save, kwargs['extra-includes'])
  
  if kwargs['list'] or (len(kwargs['list-matching']) != 0):
    print("Objects available at %s:" % extra[0])
    keys = list(cluster.keys())
    keys.sort()
    matching = None
    if len(kwargs['list-matching']) != 0: 
      matching = re.compile(kwargs['list-matching'])
    for k in keys:
      if matching and matching.search(k) is None: continue
      print(repr(cluster[k]))
      if isinstance(cluster[k], dal.Computer):
        print(' - %s, %d MB' % \
              (cluster[k].Type, cluster[k].Memory))
        print(' - %d NIC(s), %s' % \
              (len(cluster[k].Interfaces), cluster[k].HW_Tag))
      elif isinstance(cluster[k], dal.ComputerSet):
        print(' (%d Computers directly or indirectly connected)' % \
              (len(computer_list(cluster[k].Contains))))
        print_recursive(cluster[k].Contains, '  ')
      elif isinstance(cluster[k], dal.Rack):
        print(' (LFS: %s, Nodes: %d Computers directly or indirectly connected)' % \
              (cluster[k].LFS, len(computer_list(cluster[k].Nodes))))
        print_recursive(cluster[k].Nodes, '  ')

  if kwargs['count']:
    names = [k for k in list(cluster.keys()) if isinstance(cluster[k], dal.Computer)]
    racks = [k for k in list(cluster.keys()) if isinstance(cluster[k], dal.Rack)]
    print('%d machine(s), %d set(s) and %d rack(s) are described at %s' % \
          (len(names), len(cluster)-len(names)-len(racks), len(racks), extra[0]))

if __name__ == "__main__":
  main()
