#!/usr/bin/env tdaq_python
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:14:52 PM CEST 

from __future__ import print_function
import pm.project

import pm.common
from pm.dal import dal

from pm.partition.utils import parse_cmdline, post_process
from pm.segment.merging import option, gendb

import os, sys
option['partition-name']['default'] = \
  os.path.basename(sys.argv[0]).split('.', 1)[0].split('_', 1)[1]

args = parse_cmdline(gendb.__doc__, option)
d = gendb(**args)

# remove any previous version
pfile = d['partition'].id + '.data.xml'
try:
    os.remove(pfile)
    print('previous version of',pfile,' removed')
except:
    pass

d['partition'] = post_process(args, d['partition'])
p = pm.project.Project(pfile, d['includes'])
p.addObjects([d['partition']])

# add master trigger object,
# haven't yet found a way to add it before the post_process call.
mt = pm.common.create_master_trigger(p)
if mt:
    part = d['partition']
    part.MasterTrigger = mt
    p.addObjects([mt])
p.updateObjects([part])

