#!/usr/bin/env tdaq_python
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:14:52 PM CEST 

from __future__ import print_function
import os, sys
import pm.project

from pm.partition.utils import parse_cmdline, post_process
from pm.segment.hlt import option, gendb

dbg = False

args = parse_cmdline(gendb.__doc__, option)
d = gendb(**args)

if dbg:
    print('d =',d)
    print("\nbefore proxy: d['segment'] =", d['segment'])

proxy = args['proxy']
if proxy:
  templates=pm.project.Project(args['template-file'])


if dbg:
    print('d =',d)
    print("\nbefore post_process: d['segment'] =", d['segment'])

dbname = '.'.join(os.path.basename(sys.argv[0]).split('.')[:-1])
if len(d['segment']) == 1: dbname = d['segment'][0].id
d['segment'] = post_process(args, d['segment'])

if dbg:
    print("\nafter  post_process: d['segment'] =", d['segment'])

if proxy:
  pm.common.add_db_proxy(d['segment'], templates, proxy)
save_db = pm.project.Project(dbname + '.data.xml', d['includes'])

save_db.addObjects(d['segment'])
