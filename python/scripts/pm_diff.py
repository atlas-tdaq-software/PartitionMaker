#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Fri Nov  9 10:21:21 2007

"""Calculates differences between OKS databases
"""
from __future__ import print_function

import config
import logging

def obj_diff(fr, to):
  """Prints object differences"""

  diffs = 0
  # we know names and types match, so we just have to compare attrs and rels

  for k in fr.__schema__['attribute'].keys():
    if getattr(fr, k) != getattr(to, k):
      print("! %s.%s: %s -> %s" % (repr(fr), k, getattr(fr, k), getattr(to, k)))
      diffs += 1

  for k, v in fr.__schema__['relation'].items():
    orig = getattr(fr, k)
    dest = getattr(to, k)
    if v['multivalue']:
      if orig: orig = [repr(i) for i in orig]
      else: orig = '[]'
      if dest: dest = [repr(i) for i in dest]
      else: dest = '[]'
    else:
      if orig: orig = repr(orig)
      else: orig = 'NULL'
      if dest: dest = repr(dest)
      else: dest = 'NULL'
    if orig != dest:
      print("! %s.%s: %s -> %s" % (repr(fr), k, orig, dest))
      diffs += 1

    return diffs

def diff(fr, to):
  """Prints overall differences between OKS databases"""

  logging.info('Loading database (from) %s' % fr)
  from_db = config.Configuration('oksconfig:%s' % fr)
  from_objs = from_db.get_all_dals()

  logging.info('Loading database (to) %s' % to)
  to_db = config.Configuration('oksconfig:%s' % to)
  to_objs = to_db.get_all_dals()

  diffs = 0

  print("\nsection 'includes'")
  print("------------------\n")

  for k in from_db.get_includes():
    if k not in to_db.get_includes():
      print('- include: %s' % k)
      diffs += 1
  for k in to_db.get_includes():
    if k not in from_db.get_includes():
      print('+ include: %s' % k)
      diffs += 1

  print("\nsection 'new/deleted objects'")
  print("-----------------------------\n")

  for k in from_objs.keys():
    if k not in to_objs:
      print('- object: %s' % k)
      diffs += 1

  for k in to_objs.keys():
    if k not in from_objs:
      print('+ object: %s' % k)
      diffs += 1

  print("\nsection 'matching objects'")
  print("-------------------------\n")
  for k in from_objs.keys():
    if k in to_objs:
      diffs += obj_diff(from_objs[k], to_objs[k])

  return diffs

if __name__ == '__main__':
  import sys
  if len(sys.argv) != 3:
    print('usage: %s <from-database> <to-database>' % sys.argv[0])
    sys.exit(1)

  sys.exit(diff(sys.argv[1], sys.argv[2]) > 0 and 1 or 0)
