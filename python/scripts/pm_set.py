#!/usr/bin/env python3
"""
 Modify an existing OKS database.

 Instructions:

    pm_set.py [--new] [-I|--extra-includes] [-r|--reload] [-c|--commit 'COMMIT_MSG'] [-l|--log-level LEVEL] [-p|--preserve] [<backend>:]<oks_database> [<variable_file>]

 <backend> should be either "oksconfig" (for files) or "rdbconfig" (for RDB
 servers) if provided. If you omit it, "oksconfig" is assumed.

 <variable_file> can be missing, in which case pm_set.py will read from the
 standard input. All usual techniques for streaming input will work as
 expected. A few examples:

    $ echo "uid@class.relation = xyz" | pm_set.py database.data.xml

    $ pm_set.py database.data.xml <<EOF
    uid@class.relation = xyz
    EOF

 <variable_file> should be in the following syntax:

 Empty lines or lines starting with # are ignored

 To change the attribute of a single object:

    uid@class.attribute = value

 To change the attribute of all objects of a certain class:

    @class.attribute = value

 To set a relation for one or all objects of a certain class:

    uid@class.relation = uid@class
    @class.relation    = uid@class

 To add an attribute or a relation to an object or all objects of a class:

    uid@class.attribute += [ value ]
    uid@class.attribute += [ value1, value2 ]
    uid@class.relation  += [ uid@class ]
    uid@class.relation  += [ uid1@class, uid2@class ]
    @class.relation     += [ uid@class ]
 etc.

 To change the attribute or relation of a set of objects matching
 a regular expression:

    ~regexp@class.attribute = value

 To create a new object:

    uid@class

 To delete an existing object:

    !uid@class

 Examples:

    # Change all objects of this type

    @L2SVConfiguration.l2puTimeout_ms = 3000
    @PTTemplateApplication.Instances  = 8

    # Change specific objects only

    L2PU->ROS@InterfaceMap.Protocol = 'udp'
    ROS->L2PU@InterfaceMap.Protocol = 'udp'

    HLT_COMMON_PATCH_AREA@Variable.Value = [ '/home/rhauser/work', '/sw/atlas/AtlasPoint1/14.0.10.1' ]

    # Set relationsihps
    @L2PUTemplateApplication.ProcessEnvironment += [ HLT_COMMON_PATCH_AREA@Variable ]

    # Create new object, if it does not exist
    Preload@Variable

    # Modify it
    Preload@Variable.Name  = 'HLT_PRELOAD'
    Preload@Variable.Value = 'libGaudiKernel.so:libGaudiSvc.so'

    @L2PUTemplateApplication.ProcessEnvironment += [ Preload@Variable ]

    # Delete it
    ~Preload@Variable

"""
import ipc
import config
import re
import logging

def modify(db, parameters, includes, new_db, commit_msg, preserve):
    """
    Modify the OKS database 'db' with the paramaters from the opened file
    'parameters'. Addes the files in 'includes'. If new_db is true, create
    a new database from scratch.
    """
    if db[:9] not in ('oksconfig', 'rdbconfig'): db = 'oksconfig:' + db

    if new_db:
        conf = config.Configuration()
        conf.create_db(db.split(':',1)[1], includes)
    else:
        conf = config.Configuration(db)
        for inc in includes:
            conf.add_include(inc)

    modified = []

    for line in parameters.readlines():
        line = line.strip()

        logging.debug(f'line = {line}')

        if len(line) == 0 or line[0] == '#':
            continue

        rhash = line.rfind('#')
        if rhash > 0:
            line = line[:rhash]

        add    = False
        remove = False

        if line.find('+=') != -1:
            left, right = [ x.strip() for x in line.split('+=', 1) ]
            add = True
        elif line.find('-=') != -1:
            left, right = [ x.strip() for x in line.split('-=', 1) ]
            remove = True
        elif line.find('=') != -1:
            left, right = [ x.strip() for x in line.split('=',1) ]
        else:
            # Creation of new objects
            left = line.strip()
            right = ''

        logging.debug(f'left = {left}, right = {right}')

        uid, classattr = left.split('@', 1)

        rs = right.lstrip('[').rstrip(']')
        rlist  = [ x.strip() for x in rs.split(',') ]
        logging.debug(f'rs     = {rs}')
        logging.debug(f'rlist  = {rlist}')

        if len(right) > 0:
            class_name, attr = classattr.split('.', 1)
        else:
            if uid.startswith('!'):
                # delete object
                uid = uid[1:]
                try:
                    x = conf.get_obj(classattr, uid)
                    conf.destroy_obj(x)
                except Exception as ex:
                    logging.info(f'Could not remove {uid}@{classattr}')
            else:
                # create new object
                try:
                    # see if it exists
                    conf.get_obj(classattr, uid)
                except:
                    # no: create it
                    conf.create_obj(classattr, uid)
            continue

        logging.debug('{uid} {class_name,attr}')

        if len(uid) == 0:
            objects = conf.get_dals(class_name)
        elif uid.startswith('~'):
            objects = [ k for k in conf.get_dals(class_name) if re.match(uid[1:], k.id) ]
        else:
            objects = [ conf.get_dal(class_name, uid) ]

        logging.debug(f'objects = {objects}')
        logging.debug(f'add     = {add}')
        logging.debug(f'remove  = {remove}')

        for obj in objects:
            if add:
                x = getattr(obj, attr)
                to_add = []
                for r in rlist:
                    t =  eval(re.sub('([^\s]+)@([^\s]+)','conf.get_dal("\\2","\\1")',r))
                    to_add.append(t)
                for i in to_add:
                    if not preserve:
                        if i in x:
                            x.remove(i)
                x += to_add
                setattr(obj, attr, x)
            elif remove:
                try:
                    x = getattr(obj, attr)
                    for r in rlist:
                        x.remove(eval(re.sub('([^\s]+)@([^\s]+)','conf.get_dal("\\2","\\1")',r)))
                    setattr(obj, attr, x)
                except:
                    pass
            else:
                to_set = []
                for r in rlist:
                    t =  eval(re.sub('([^\s]+)@([^\s]+)','conf.get_dal("\\2","\\1")',r))
                    to_set.append(t)
                try:
                    setattr(obj, attr, to_set)
                except ValueError as a:
                    setattr(obj, attr, to_set[0])
                    # print obj
            conf.update_dal(obj)

    conf.commit(commit_msg)

if __name__ == '__main__':

  import sys
  from EventApps import myopt

  option = {
      'reload': {'short': 'r', 'arg': False, 'default': None,
                 'description': 'issues a full database reload after a commit'},
      'extra-includes': { 'short': 'I', 'arg': True, 'default': [],
                          'description': 'Extra OKS include file' },
      'new': { 'short': 'n', 'arg': False, 'default': None,
               'description': 'create a new database from scratch (only useful with appropriate input to -I)' },
      'commit': { 'short': 'c', 'arg': True, 'default': 'Modified by pm_set.py',
                  'description': 'commit messages for changes' },
      'log-level': { 'short': 'l', 'arg': True, 'default': 'WARN',
                     'description': 'set log level' },
      'preserve': { 'short': 'p', 'arg': False, 'default': None,
                    'description': 'when appending, do not make sure entries are unique' }
  }

  parser = myopt.Parser(extra_args=True, post_options=__doc__)
  for k,v in option.items():
      parser.add_option(k, v['short'], v['description'], v['arg'], v['default'])

  # process arguments
  kwargs, extra = parser.parse(sys.argv[1:], prefix='"%s" options:' % sys.argv[0])

  if len(extra) == 0 or len(extra) > 2:
      print(parser.usage())
      sys.exit(1)

  if len(extra) == 1: input1 = sys.stdin
  else: input1 = open(extra[1], 'rt')

  logging.basicConfig(level = getattr(logging, kwargs['log-level']))

  modify(extra[0], input1, kwargs['extra-includes'], kwargs['new'], kwargs['commit'], kwargs['preserve'])

  # if the user asks, we issue an db reload, if the server is an RDB server.
  if kwargs['reload']:
      import re, subprocess
      r = re.search(r'rdbconfig:(?P<server>.+)@(?P<partition>.+)', extra[0])
      if not r:
          logging.error('Can only reload RDB servers and "%s" does not look like one. Ignoring user request.' % extra[0])
      else:
          cmd = ['rdb_admin', '--partition', r.group('partition'),
                 '--database', r.group('server'), '--reload', 'partition']
          status = subprocess.check_call(cmd)
