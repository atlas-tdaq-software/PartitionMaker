#!/usr/bin/env tdaq_python

from __future__ import print_function
dbg = True

import os, sys
from distutils.file_util import copy_file
import pm.project
from pm.utils import safeInsert
from pm.multinode import localhost

infile   = 'HLT.data.xml'
if len(sys.argv) > 1:
    infile = sys.argv[1]

try:
    db = os.environ['TDAQ_DB_USER_REPOSITORY'] + os.sep
    print('using TDAQ_DB_USER_REPOSITORY =',db)
except KeyError:
    try:
        db = os.environ['USER_REPOSITORY'] + os.sep
        print('using USER_REPOSITORY =',db)
    except KeyError:
        print('ERROR: neither TDAQ_DB_USER_REPOSITORY nor USER_REPOSITORY are defined')
        sys.exit(1)
#
print('\ndb =',db)
#
# temporary names so as not to conflict with what's already in the /atlas/oks/ db
#
outfile1 = db + 'daq/segments/HLT/HLT-top.data.xml'
outfile2 = db + 'daq/segments/HLT/HLT-int.data.xml'
#
# save a copy of infile
#
copy_file(infile,infile+'.save')
#
# first generate the -internal segment,
# by deleting HLT-Segment-1
#
copy_file(infile,outfile2)
#
p2 = pm.project.Project(outfile2)
objs_to_remove = [ p2.get_dal('Segment','HLT') ]
p2.removeObjects(objs_to_remove)
p2.commit()
#
# now generate the top HLT segment which includes the above
# and also uses the RoIB stuff etc
#
p0 = pm.project.Project(infile)
hltseg = p0.getObject('Segment','HLT')

incs = ["daq/sw/repository.data.xml",
        "daq/schema/df.schema.xml" ]

incs2 = "daq/hw/hosts-onl.data.xml"
try:
    px = pm.project.Project(incs2)
    incs.append(incs2)
    del px
except RuntimeError:
    print('did not find file ',incs2)
    pass

incs3 = "daq/hw/hosts-mon.data.xml"
try:    
    px = pm.project.Project(incs3)
    incs.append(incs3)
    del px
except RuntimeError:
    print('did not find file ',incs3)
    pass

incs4 = [ "daq/segments/HLT/HLT-int.data.xml",
          "daq/segments/HLT/RoIBSegment_LVL1.data.xml"]
incs = incs + incs4

if dbg:
    print('\nlist of includes:')
    i = 0
    for l in incs:
        i += 1
        print('%2d %s' % (i,l))
    print()
    print('localhost ',localhost)
    print()
#
p1 = pm.project.Project(outfile1,[])
p1.addInclude(incs)
p1.commit()         # required after addInclude
#
# add RoIB & ResourceSets
#
roib       = p1.getObject('RoIBApplication','ROIB-1')
roibInput  = p1.getObject('ResourceSet','RoIBInput')
roibOutput = p1.getObject('ResourceSet','RoIBOutput')
safeInsert(hltseg,'Applications',[roib])
safeInsert(hltseg,'Resources',[roibInput,roibOutput])
p1.addObjects([hltseg])
#
# now we have to fixup the temporary names and the included file names
#
copy_file(outfile1,'./HLT.data.xml.tmp')
copy_file(outfile2,'./HLT-internal.data.xml')

#s1 = db + 'daq/segments/HLT/HLT-int.data.xml'
s1 =      'daq/segments/HLT/HLT-int.data.xml'
s2 =      'daq/segments/HLT/HLT-internal.data.xml'
if dbg:
    print('s1: ',s1)
    print('s2: ',s2)
f = open('./HLT.data.xml.tmp')
li = f.readlines()
f.close()

lo = []
for l in li:
    l1 = l
    l = l.replace(s1,s2)
    l2 = l
    if dbg:
        if l1 != l2:
            print('l1: ',l1)
            print('l2: ',l2)
    lo.append(l)
f = open('./HLT.data.xml','w')
f.writelines(lo)
f.close()
#
# clean up
#
if dbg:
    print('removing ',outfile1)
os.remove(outfile1)
if dbg:
    print('removing ',outfile2)
os.remove(outfile2)
if dbg:
    print('removing ./HLT.data.xml.tmp')
os.remove('./HLT.data.xml.tmp')
