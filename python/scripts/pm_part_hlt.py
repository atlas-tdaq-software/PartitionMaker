#!/usr/bin/env tdaq_python
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Tue 21 Aug 2007 12:14:52 PM CEST 

from __future__ import print_function
dbg = False

import sys, os
#
# tell PM we are generating EVOLUTION stuff
#
import pm.project

import pm.common
from pm.dal import  HLTSVdal, HLTMPPUdal, PUDUMMYdal, DFdal, dal

from pm.partition.utils import parse_cmdline, post_process
from pm.partition.hlt import option, gendb

try:
    def_partname = os.path.basename(sys.argv[0]).split('.', 1)[0].split('_', 1)[1] + '_' + os.getlogin()
except OSError:
    def_partname = 'part_hlt'

option['partition-name']['default'] = def_partname

args = parse_cmdline(gendb.__doc__, option)
if dbg: 
    print('pm_part_hlt.py: args=\n',args)
    print("args['hlt-farm']  =",args['hlt-farm'])
    print("args['proxy']     =",args['proxy'])

d = gendb(**args)

if dbg: print('after gendb: d =\n',d)

proxy = args['proxy']
if dbg: 
  print('proxy =',proxy)
if proxy:
  templates = pm.project.Project(args['template-file'])

# remove any previous version
pfile = d['partition'].id + '.data.xml'

try:
    os.remove(pfile)
    print('previous version of',pfile,' removed')
except:
    pass

if proxy:
  pm.common.add_db_proxy(d['partition'], templates, proxy)

p = pm.project.Project(pfile, d['includes'])

if dbg:
  print('before addObjects: d =\n',d)
  print('before addObjects: dir(p) =\n',dir(p))
  print('before addObjects: p.classes() =\n',p.classes())

d['partition'] = post_process(args, d['partition'])

p.addObjects([d['partition']])

# add master trigger object

mt = pm.common.create_master_trigger(p)
if mt:
    part = d['partition']
    part.MasterTrigger = mt
    p.addObjects([mt])
    p.updateObjects([part])

