#!/bin/bash
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Fri 23 Nov 2007 12:24:50 PM CET
source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh $CMTRELEASE $CMTCONFIG
#
export PYTHONPATH=.:${TDAQ_PYTHONPATH}
export TDAQ_DB_PATH=.:${TDAQ_DB_PATH}

echo "current host: `hostname`"
echo "current dir : `pwd`"

# local testing before new nightly (NOT for check target)
if [ "$USER" = "haimo" ]; then
    export PATH=~haimo/public/tbed/nightly/installed/share/bin:$PATH
    export PYTHONPATH=~haimo/public/tbed/nightly/installed/share/lib/python:$PYTHONPATH
    export TDAQ_PYTHONPATH=$PYTHONPATH

    ~haimo/bin/listpath PATH
    ~haimo/bin/listpath LD_LIBRARY_PATH
    ~haimo/bin/listpath PYTHONPATH
    ~haimo/bin/listpath TDAQ_PYTHONPATH
    ~haimo/bin/listpath TDAQ_DB_PATH
fi

echo "number of args = $#"
if [ $# -lt 1 ]; then
  target="all"
else
  target=$1
fi
echo "target = $target"
#
cd ../python/examples && make --jobs=1 $target;
exit $?
